"""spa_booking URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.conf.urls.i18n import i18n_patterns


urlpatterns = [
    path('api/', include('hotel_app.urls')),
    path('api/', include('user_app.api_urls')),
    path('api/', include('service_app.api_urls')),
    path('api/', include('order_app.api_urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
    path('', include('info_app.urls')),
    path('account/', include('user_app.urls')),
    path('book/', include('service_app.urls'), name="book"),
    path('pdf/', TemplateView.as_view(template_name='user_app/pdf_spa_booking.html'))
]

urlpatterns += i18n_patterns(
    # path('account/', TemplateView.as_view(template_name='apps/account.html'), name="account"),
    # path('admin_account/', TemplateView.as_view(template_name='apps/admin-account.html'), name="admin-account"),
    # path('book2/', TemplateView.as_view(template_name='apps/book2.html'), name="book2"),
    # path('book3/', TemplateView.as_view(template_name='apps/book3.html'), name="book3"),
    # path('book_admin/', TemplateView.as_view(template_name='apps/book-admin.html'), name="book-admin"),
    # path('therapist_account/', TemplateView.as_view(template_name='apps/therapist-account.html'), name="therapist-account"),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
                 + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
