from hotel_app.models import TherapistModel, HotelAdministratorModel
from info_app.models import FooterInfoModel


def user_type(user):
    is_therapist = TherapistModel.objects.filter(email=user.email).exists()
    is_administrator = HotelAdministratorModel.objects.filter(email=user.email).exists()
    is_superuser = is_therapist == is_administrator
    if is_therapist:
        _user = TherapistModel.objects.filter(email=user.email).first()
    elif is_administrator:
        _user = HotelAdministratorModel.objects.filter(email=user.email).first()
    else:
        _user = user
    data = dict(
        {
            'is_therapist': is_therapist,
            'is_administrator': is_administrator,
            'is_superuser': is_superuser,
            'user': _user
        }
    )
    return data


def get_footer_info():
    return FooterInfoModel.objects.first()