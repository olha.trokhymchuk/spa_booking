from .settings import *

ALLOWED_HOSTS += ['185.65.245.59']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'spa_booking_dev_db',
        'USER': 'spa_booking',
        'PASSWORD': 'datasvitend',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
