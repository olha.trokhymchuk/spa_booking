from django.contrib.auth.views import LogoutView
from django.urls import path

from .service.service import render_to_pdf, render_to_pdf_filter
from .views import *

urlpatterns = [
    path('', UserAccountView.as_view(), name="account"),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('render_to_pdf/', render_to_pdf_filter, name='render_to_pdf')
    # path('contact_us/', ContactUs.as_view(), name="contact_us"),
    # path('login/', CustomLoginView.as_view(), name="login"),


]
