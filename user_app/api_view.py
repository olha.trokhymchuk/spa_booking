from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from spa_booking.custom_api_exeptions import Http400
from spa_booking.helper import user_type
from .serializers import *
from .models import *


class UserUpdateApiView(generics.UpdateAPIView):
    def get_object(self):
        if 'id' not in self.request.data:
            raise Http400(detail="id - required field.")

        _id = self.request.data.get("id")
        return user_type(get_object_or_404(UserModel.objects.all(), id=_id)).get('user')

    def get_serializer_class(self):
        type_of_user = user_type(self.get_object())
        if type_of_user.get('is_therapist'):
            return TherapistSerializer
        elif type_of_user.get('is_administrator'):
            return HotelAdministratorSerializer
        else:
            return UserModelSerializer

    def perform_update(self, serializer):
        serializer.save()
        if 'password' in serializer.validated_data:
            instance = self.get_object()
            instance.set_password(serializer.validated_data.get('password'))
            instance.save()
            # print(instance.password)




