from django.urls import path
from .api_view import *

urlpatterns = [
    path('update_user/', UserUpdateApiView.as_view(), name="update_user"),

]
