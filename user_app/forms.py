from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import EmailField
from .models import UserModel


class UserModelCreationForm(UserCreationForm):
    email = EmailField(required=True)

    class Meta(UserCreationForm.Meta):
        model = UserModel
        fields = ('email', 'password1', 'password2')


class UserModelChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = UserModel
        exclude = ('username',)
