from rest_framework import serializers
from hotel_app.models import TherapistModel, HotelAdministratorModel
from .models import UserModel


class TherapistSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False, write_only=True)

    class Meta:
        model = TherapistModel
        fields = ('id', 'first_name', 'last_name', 'email', 'img', 'password')


class HotelAdministratorSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False, write_only=True)

    class Meta:
        model = HotelAdministratorModel
        fields = ('id', 'first_name', 'last_name', 'email', 'img', 'password')


class UserModelSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False, write_only=True)

    class Meta:
        model = UserModel
        fields = ('id', 'first_name', 'last_name', 'email', 'password')
