from weasyprint.text.fonts import FontConfiguration
from django.http import HttpResponse
from order_app.models import OrderModel
from weasyprint import HTML, CSS
from django.conf import settings
from django.template.loader import get_template
import django_filters


def render_pdf(orders):
    template = get_template('user_app/pdf_spa_booking.html')
    render_html = template.render({"orders": orders})
    pdf_file = HTML(string=render_html). \
        write_pdf(stylesheets=[CSS(str(settings.BASE_DIR) + '/static/app/css/spa_booking_pdf.css')], zoom=1.1)
    return pdf_file


font_config = FontConfiguration()
def render_to_pdf(request):
    user = request.user
    name_pdf = 'Spa booking'
    orders = OrderModel.objects.filter(therapist=user)
    response = HttpResponse(render_pdf(orders), content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(name_pdf)
    return response


class OrderFilter(django_filters.FilterSet):
    book_data_time = django_filters.DateFilter(field_name='book_data_time', lookup_expr='date')

    class Meta:
        model = OrderModel
        fields = ['hotel', 'status', 'hotel_admin', 'therapist', 'book_data_time', 'pay_by_cart', 'canceled']


def render_to_pdf_filter(request):
    name_pdf = 'Spa booking'
    queryset = OrderFilter(request.GET, queryset=OrderModel.objects.all()).qs
    response = HttpResponse(render_pdf(queryset), content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(name_pdf)
    return response
