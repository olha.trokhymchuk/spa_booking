from django.contrib.auth.models import AbstractUser
from django.db import models
from user_app.managers import UserModelManager
from django.utils.translation import gettext as _


class UserModel(AbstractUser):
    username = None
    email = models.EmailField(_('Email address'), blank=True, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserModelManager()

    class Meta(AbstractUser.Meta):
        db_table = 'users_base'
        verbose_name = _('User base')
        verbose_name_plural = _('Users base')
        swappable = 'AUTH_USER_MODEL'
