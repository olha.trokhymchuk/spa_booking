from django.contrib import admin

from .forms import UserModelCreationForm, UserModelChangeForm
from .models import UserModel
from django.contrib.auth.admin import UserAdmin


class UserModelAdmin(UserAdmin):
    add_form = UserModelCreationForm
    form = UserModelChangeForm
    exclude = ('username',)
    ordering = ('email', )
    list_display = ('id', 'email', )
    list_display_links = ('id', 'email', )
    search_fields = ('email', )

    fieldsets = (
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'password')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', )}),
    )

    add_fieldsets = (
        ('Credentials', {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', )}),
    )


admin.site.register(UserModel, UserModelAdmin)
