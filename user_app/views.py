from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse
from django.views import generic
from hotel_app.models import TherapistModel, HotelAdministratorModel, HotelModel
from info_app.models import ContactModel
from spa_booking.helper import user_type, get_footer_info
from .models import UserModel


class UserAccountView(LoginRequiredMixin, generic.TemplateView):
    template_name = "user_app/account.html"

    def get_login_url(self):
        return reverse("contact_us")

    def get_context_data(self, **kwargs):
        context = super(UserAccountView, self).get_context_data(**kwargs)

        _user_type = user_type(self.request.user)
        if _user_type.get('is_superuser'):
            context.update({'hotels': HotelModel.objects.all()})

        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'user_type': _user_type}
        )

        return context

