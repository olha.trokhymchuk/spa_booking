$(document).ready(function () {
//   setDefaultValue();
  setValuesByClickingOnOption();
  changeDataForAdditionalService();
});

function setDefaultValue() {
  let id = $("#id_service").find("[selected]").val();
  let priceContainer = $("#id_price");
  let durationContainer = $("#id_minutes");

  fetch(`${document.location.origin}/api/service/${id}/`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      priceContainer.val(data.price);
      durationContainer.val(data.minutes);
    });
}

function setValuesByClickingOnOption() {
  $(document).on("change", ".form-row.field-service select", function () {      
    let id = $(this).val();
    let priceContainer = $("#id_price");
    let durationContainer = $("#id_minutes");

    fetch(`${document.location.origin}/api/service/${id}/`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        priceContainer.val(data.price);
        durationContainer.val(data.minutes);
      });
  });
}

function changeDataForAdditionalService() {
  $(document).on("change", "#additional_services-group .dynamic-additional_services select", function () {

    let id = $(this).val();
    let eppriceContainer = $(this).closest('#additional_services-group .dynamic-additional_services').find('.field-price input');
    console.log(eppriceContainer)

    fetch(`${document.location.origin}/api/add_service/${id}/`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        eppriceContainer.val(data.price);
      })
  });
}
