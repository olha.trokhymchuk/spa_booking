document.addEventListener("DOMContentLoaded", () => {
  clearDataFromLocalStorage();

  getDateByClickingOnCell();
  getHotels();
  setActiveAdditionalLabel();
  sessionStorage.setItem("price", "0");

  // changeDay();

  openCustomTimePicker();
  calculateData();
  proceedToCheckout();
  // sendDataToApi();
  triggerValidation();
  sendDataToPaymantService();
  changePageByPaginationClick();
  filterBookingList();
  getBookingListByState();
  filterByData();
  relocateByClick();
  showHiddenList();
  drillGetParametresToGeneratePdfButton();
  handlerForPromotionButton();
  validateForm() 


  $(".datepicker-here").datepicker({
    minDate: new Date(),
  });

  if ($("body").hasClass("hash")) {
    $(window.location.hash).addClass("active");
    if ($(window.location.hash).length > 0) {
      $(".containerWrapper__One")
        .addClass("in-view")
        .siblings()
        .removeClass("in-view");
    }
    $(".greetings").click(function () {
      $(".sub-steps").removeClass("active");
      $(".containerWrapper__Two")
        .addClass("in-view")
        .siblings()
        .removeClass("in-view");
      $(".steps li").removeClass("active");
    });
  }

  // account buttons

  $(".edit").click(function () {
    var currentInput = $(this).parent().next("input");
    var buttons = $(this).parent().next().next(".short");
    buttons.addClass("visible");
    $(this).parent().parent().addClass("editable");
    $(this).focus();
    currentInput.addClass("editable");
    if (currentInput.hasClass("editable")) {
      currentInput.prop("readonly", false);
    } else currentInput.prop("readonly", true);

    $(".deliverEdit").removeAttr("disabled");
  });
  $(".close").click(function () {
    var buttons = $(this).parent().next().next(".short");
    var currentInput = $(this).parent().next("input");
    currentInput.removeClass("editable");
    $(this).parent().parent().removeClass("editable");
    buttons.removeClass("visible");
    $(".deliverEdit").attr("disabled", true);
  });
  $(".button").click(function () {
    $(this).parents(".AASwrapper").removeClass("editable");
    $(this).parents(".short").removeClass("visible");
    let data = $(this).parents(".AASwrapper").find("input").val();
    let className = $(this).parents(".AASwrapper").find("input").attr("class");

    if (
      $(this).hasClass("nameClose") ||
      $(this).hasClass("emailClose") ||
      $(this).hasClass("passwordClose")
    ) {
      let obj = {
        id: gettedSuperData.userId,
      };

      switch (className.split(" ")[0]) {
        case "nameEdit":
          let firstName = data.split(" ")[0];
          let lastName = data.split(" ")[1];

          obj.first_name = firstName;
          obj.last_name = lastName;
          break;
        case "emailEdit":
          let email = data;
          obj.email = email;
          break;
        case "passwordEdit":
          let password = data;
          obj.password = password;
          break;
        default:
          break;
      }


      fetch(`${document.location.origin}/api/update_user/`, {
        headers: {
          "Content-Type": "application/json",
          "X-CSRFToken": csrf,
        },
        method: "put",
        credentials: "same-origin",
        body: JSON.stringify(obj),
      });
    }
  });

  $(".nextArrow").click(function () {
    $(".globalCalendar.visible")
      .next()
      .addClass("visible")
      .siblings()
      .removeClass("visible");
    if ($(".globalCalendar:not(.visible)").length == 11) {
      $(".globalCalendar").eq(0).addClass("visible");
    }
  });
  $(".prevArrow").click(function () {
    $(".globalCalendar.visible")
      .prev()
      .addClass("visible")
      .siblings()
      .removeClass("visible");
    if ($(".globalCalendar").eq(0).hasClass("visible")) {
      $(".globalCalendar").removeClass("visible");
      $(".globalCalendar").eq(10).addClass("visible");
    }
  });

  $(document).click(function (event) {
    var $target = $(event.target);
    if ($(event.target).closest(".guests").length == 0) {
      $(".function").removeClass("active");
    }
  });

  if ($("body").hasClass("haveSlider")) {
    if ($(window).width() < 1025) {
      $(".slider").slick({
        centerMode: true,
        centerPadding: "60px",
        slidesToShow: 1,
        arrows: false,
        responsive: [
          {
            breakpoint: 1025,
            settings: {
              arrows: false,
              dots: true,
              centerMode: true,
              centerPadding: "0px",
              slidesToShow: 1,
            },
          },
        ],
      });
    }
    $(".slider3").slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      dots: true,
      centerPadding: "0px",
      arrows: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            centerMode: true,
            centerPadding: "0px",
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    if ($(window).width() < 768) {
      $(".slider3").on("afterChange", function (event, slick, currentSlide) {
        if ($(".slider3 .slick-active").find("li").hasClass("checkbox-item")) {
          $(".checkbox-section").addClass("visible");
          $(".checkbox-section").addClass("non-clickable");
        } else {
          $(".checkbox-section").removeClass("visible");
        }

        if ($(".slider3 .slick-active").find("li").hasClass("active")) {
          $(".checkbox-section").removeClass("non-clickable");
        }
      });
    }
    $(".slider4").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      centerPadding: "0px",
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }
  $(".input").on("input", function () {
    if ($("#confirmation").val().length > 0 && $("#address").val().length > 0) {
      $(".deliverButton").removeClass("disabled");
    } else $(".deliverButton").addClass("disabled");
  });

  $(".toContactUs").click(function () {
    window.location.href = "contactUs.html";
  });

  $(".section2 .cardsList__item, #generateSnake ").click(function () {
    if ($(this).hasClass("checkbox-item")) {
      $(".checkbox-section").addClass("visible");
      $(".checkbox-section").removeClass("non-clickable");
    } else {
      // $(".checkbox-section").removeClass("visible");
      $(".checkbox-section").addClass("non-clickable");
    }
    $(".section2 .cardsList__item").removeClass("active");
    $(this).addClass("active");
    if ($(".additional-section .cardsList__item").hasClass("active")) {
      $(".additional-next").removeClass("disabled");
    }
    if ($(".third-step .cardsList__item").hasClass("active")) {
      $(".next2").removeClass("disabled");
    }

    getCurrentData(sessionStorage.getItem("date"));
    changeDay(sessionStorage.getItem("date"));
    setCustomTimeByClick();
    let currentElem = $(this).find(".cardText input");

    showAdditionalServices(currentElem);
  });

  $(".form label").click(function () {
    if ($(".timepicker-on").is(":checked")) {
      $(".hide").addClass("visible");
    } else {
      $(".hide").removeClass("visible");
    }
    if ($(this).hasClass("active")) {
      $(".next3").removeClass("disabled");
    }
  });

  $("#payment-checkbox").on("change", function () {
    $(this).toggleClass("active");
    if ($(this).is(":checked")) {
      $(".payment-labels").addClass("active");
    } else {
      $(".payment-labels").removeClass("active");
    }
  });

  $("#adults").on("change", function () {
    if ($(this).val() == 0) {
      $(this).prev().addClass("disabled");
    } else $(this).prev().removeClass("disabled");
  });
  $("#children").on("change", function () {
    if ($(this).val() == 0) {
      $(this).prev().addClass("disabled");
    } else $(this).prev().removeClass("disabled");
  });

  $(".openCalc").click(function () {
    $(".function").toggleClass("active");
  });

  var hoursSelection = "";
  var minutesSelection = "";
  var i = 0;
  for (var i = 0; i <= 12; i++) {
    hoursSelection +=
      "<option value='" +
      zeroFill(i, 2) +
      "01'>" +
      zeroFill(i, 2) +
      "</option>";
  }
  for (var i = 0; i <= 60; i++) {
    minutesSelection +=
      "<option value='" +
      zeroFill(i, 2) +
      "00'>" +
      zeroFill(i, 2) +
      "</option>";
  }
  $("#hours").html(hoursSelection);
  $("#minutes").html(minutesSelection);

  function zeroFill(number, width) {
    width -= number.toString().length;
    if (width > 0) {
      return new Array(width + (/\./.test(number) ? 2 : 1)).join("0") + number;
    }
    return number + ""; // always return a string
  }

  $(".calendarWrapper li").click(function () {
    $(".calendarWrapper li").removeClass("active");
    $(this).addClass("active");
  });

  if ($("#adults").val() == 0) {
    $("#adults").addClass("disabled");
  } else $("#adults").removeClass("disabled");

  $(".calc").click(function () {
    var val = $(this).siblings("input").val();

    if ($(this).val() == "plus") {
      val++;
    } else if (val > 0) {
      val--;
    }
    $(this).siblings("input").val(val).trigger("change");
    var adults = $("#adults").val();
    var children = $("#children").val();
    $("#adultDigit").text(adults);
    $("#childrenDigit").text(children);
  });
  $(document).on("keyup", function (e) {
    if (e.keyCode === 13) {
      $(".function").removeClass("active");
    }
  });
  if ($(".function").hasClass("active")) {
    $(document).on("click", function (e) {
      if ($(e.target).is(".function") === false) {
        $(".function").addClass("active2");
      }
    });
  }

  $(".next").click(function (e) {
    e.preventDefault();
    var index = $(this).attr("index");

    $(this).parent().removeClass("visible").next().addClass("visible");
    $(".slider3").slick("setPosition");
    $(".generalSteps li").removeClass("active").eq(index).addClass("active");
    $(".generalSteps li").eq(index).addClass("finished");

    var that = $(this);
    $("html, body").animate({
      scrollTop: $(".title").offset().top + 200,
    });
  });
  $(".generalSteps li").click(function () {
    var index = $(this).attr("data-index");
    $(this).addClass("active").siblings().removeClass("active");

    $(".mainContent--change .sections")
      .removeClass("visible")
      .eq(index)
      .addClass("visible");
    $(".slider3").slick("setPosition");
  });

  // $('.specTable li .table').click(function() {
  // 	if (!$(this).parent().hasClass('active')) {
  // 	    $(this).parent().find('.animated').slideDown();
  // 	} else
  // 	    $(this).parent().find('.animated').slideUp();
  // 	})

  $(".checkbox__form input").click(function () {
    if ($(this).prop("checked") == true) {
      $(".checkbox__form label").removeClass("active");
      $(this).parent("label").addClass("active");
    }
  });

  $(".steps li").click(function () {
    let url = "";
    sessionStorage.setItem("book_data_time", "");

    switch (gettedSuperData.userRole) {
      case "superuser":
        url = `${document.location.origin}/api/orders/`;
        break;
      case "hotel_admin":
        url = `${document.location.origin}/api/orders?&hotel_admin=${gettedSuperData.userId}`;
        break;
      case "therapist":
        url = `${document.location.origin}/api/orders?&therapist=${gettedSuperData.userId}`;
        break;
      default:
        break;
    }

    sessionStorage.setItem("filter", url);

    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        generateBookingList(data, 1);
      });
    $(".sub-steps li").removeClass("active");
    $(this).addClass("active").siblings().removeClass("active");
    $(".tableWrapper")
      .removeClass("in-view")
      .eq($(this).attr("data-index"))
      .addClass("in-view");
    if ($("#bookinglist").hasClass("active")) {
      $(".sub-steps").addClass("active");
    } else $(".sub-steps").removeClass("active");
  });

  $(".sub-steps li").click(function () {
    let url;
    let id = $(this).data();
    let filter = sessionStorage.getItem("filter");
    sessionStorage.setItem("hotelID", id.id);

    filter === `${document.location.origin}/api/orders/`
      ? (url = `${filter}?hotel=${id.id}`)
      : (url = `${filter}&hotel=${id.id}`);

    // sessionStorage.setItem("filter", url)
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        generateBookingList(data, 1);
      });

    $(this).addClass("active").siblings().removeClass("active");
  });
  $(".preTable li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    if ($("#AC").hasClass("active")) {
      $(".admin-version .button-container").addClass("multiple");
    } else $(".admin-version .button-container").removeClass("multiple");
  });
  $(".AAS li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    $(".AAScontent")
      .removeClass("in-view")
      .eq($(this).index())
      .addClass("in-view");
  });

  // GALLERY

  $(".play").click(function (e) {
    e.preventDefault();
    $(".mainVideo").trigger("play");
    $(".mainVideo").prop("controls", true);
  });

  $(".videoPart__gallery li").click(function () {
    // $(this).addClass('active').siblings().removeClass('active');
    $(".videoPart__text")
      .removeClass("active")
      .eq($(this).index())
      .addClass("active");
    $(".videoPart__video img")
      .removeClass("active")
      .eq($(this).index())
      .addClass("active");
  });

  if ($("body").hasClass("haveDatePicker")) {
    var checkin = document.querySelectorAll("#checkin");
    var checkoff = document.querySelectorAll("#checkoff");
    const picker = datepicker("#checkin", {
      // Event callbacks.
      onSelect: (instance) => {
        $("#checkoff").next().removeClass("qs-hidden");
      },
    });

    checkoff.forEach(function (el) {
      datepicker(el);
    });
  }

  // if ($("body").hasClass("haveSelect")) {
  //   $(".select").select2();
  // }
});

function generateBookingList(data, number) {
  let container = $(".headings.specTable");
  let paginationContainer = $("#pagination ul");
  let lang = $(".lang_de.selected_e").val().toUpperCase();

  data.results != 0
    ? container.html(
        data.results.map((d) => {
          let additional = d.additional_service_json;
          let translations = additional.map((a) => {
            return a.id != null ? a.translations : { lang: "" };
          });

          let filteredTransltations = translations
            .flat()
            .filter((i) => i.lang == lang);

          return `
      <li class="main-li" id='${d.id}'>
      <ul class="table">
          <li class="active">
              <h5># ${d.id}</h5>
              <span style="text-transform: uppercase" class="btn ${d.status}">${
            d.status
          }</span>
              ${
                d.pay_by_card
                  ? ""
                  : `<span class="btn btnContract">CONTRACT</span>`
              }

              ${
                d.canceled
                  ? `<span class="btn btnCancelled">CANCELLED</span>`
                  : ""
              }

          </li>
          <li>
              <h5>Date / time</h5>
              <span>${d.book_data_time.slice(0, 19).replace("T", "/")}</span>
          </li>
          <li>
              <h5>Duration</h5>
              <span>${d.service_json.minutes} min.</span>
          </li>
          <li>
              <h5>Treatment</h5>
              <span>${d.service_json.translations[0].title}</span>
          </li>
          <li>
              <h5>Order price</h5>
              <span>${d.total_price}</span>
          </li>
      </ul>
      <div class="animated" style="display: none;">
          <ul class="tableInside admin-mode">
              <li class="another-fontsize">
                  <h5>Order information</h5>
                  <span>${d.client_first_name} ${d.client_last_name}</span>
                  <span>${d.client_email}</span>
                  <span>${d.client_room} room</span>
              </li>
              <li>
                  <h5>Date of order creation</h5>
                  <span>${d.created.slice(0, 19).replace("T", "/")}</span>
              </li>
              <li>
                  <h5>Hotel Worker Name</h5>
                  <span>${
                    d.hotel_admin != null
                      ? [
                          d.hotel_admin.first_name +
                            "" +
                            d.hotel_admin.last_name,
                        ]
                      : "-"
                  }</span>
              </li>
              
                  <li class="">
                    <div class="select-container">
                    <h5>Set up therapist</h5>
                    <p> </p>
                    ${
                      gettedSuperData.userRole == "superuser"
                        ? "<select> </select>"
                        : [
                            d.therapist != null
                              ? d.therapist.first_name +
                                "" +
                                d.therapist.last_name
                              : "-",
                          ]
                    }
                    
                </div>

                  </li>
              

              <li class="another-fontsize">
                  <h5>Additions</h5>
                  <span>${filteredTransltations.map((f) => f.title)}</span>
              </li>
              <li class="another-fontsize">
                  <h5>Comment</h5>
                  <span>${d.comment == "" ? "No comment" : d.comment}

                  </span>
              </li>
              
              <li class="button-container">
                    ${
                      gettedSuperData.userRole !== "therapist"
                        ? '<a href="#" class="button ">cancel the order</a>'
                        : ""
                    }
                    
              </li>
          </ul>
      </div>
  </li>
      `;
        })
      )
    : container.html('<h2 style="text-align: center"> No bookings </h2>');

  // let select = $(".select-container > select");
  let bookings = $(".headings.specTable .main-li");
  [...bookings].forEach((b) => {
    let select;
    let id = $(b).attr("id");
    $(b).find(".btn.awaiting_confirmation").length > 0
      ? (select = $(b).find(".select-container > select"))
      : $(b).find(".select-container > p").text("therapist");
    getTherapistData(id, select);
  });

  let pagination = [];

  for (let i = 1; i <= data.page_num; i++) {
    pagination.push(i);
  }

  paginationContainer.html(
    pagination.map((p, index) => {
      return index + 1 == number
        ? `<li><span class="active">${p}</span></li>`
        : `<li><span>${p}</span></li>`;
    })
  );

  $(".specTable > .main-li").click(function (event) {
    event.preventDefault();
    if (!event.target.closest(".select-container")) {
      $(this).find("h4").toggleClass("active");
      $(this).toggleClass("active");
      $(this).siblings().find(".animated").slideUp();
      $(this).siblings().removeClass("active");
    }

    if ($(this).hasClass("active")) {
      $(this).find(".animated").slideDown();
    } else $(this).find(".animated").slideUp();
  });
}

let option = "";

function getTherapistData(id, container) {
  fetch(`${document.location.origin}/api/therapists?order_id=${id}`)
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
    })
    .then((data) => {
      if (container) {
        container.html(
          (option = data.map((d) => {
            return `<option>${d.email}</option>`;
          }))
        );
      }
      return data;
    })
    .then((data) => {
      if (data.length) {
        $(container).addClass("select2");
        $(".select2").select2();
      }
    });
}

function changePageByPaginationClick() {
  $(document).on("click", "#pagination ul li", function () {
    let numberOfPage = $(this).find("span").text();
    let book_data_time = sessionStorage.getItem("book_data_time");
    let filter = sessionStorage.getItem("filter");
    let query;

    filter === `${document.location.origin}/api/orders/`
      ? (query = `${sessionStorage.getItem("filter")}?page=${numberOfPage}`)
      : (query = `${sessionStorage.getItem("filter")}&page=${numberOfPage}`);

    fetch(query)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        generateBookingList(data, numberOfPage);
      });
  });
}

function getDateByClickingOnCell() {
  $(document).on("click", ".datepicker--cell", function () {
    const dateObj = $(this).data();
    const settedDate = new Date(
      `${dateObj.month + 1} ${dateObj.date}, ${dateObj.year}`
    );
    const pickToggle = $(
      ".calendarWrapper .datepicker--cell.datepicker--cell-day.-selected-"
    ).length;
    sessionStorage.setItem("date", settedDate);

    pickToggle > 0
      ? $(".button.noHover.next.next1.disabled").removeClass("disabled")
      : $(".button.noHover.next.next1").addClass("disabled");

    $(
      ".book3 .mainContent--change .section2 .cardsList__item.active, .book3 .mainContent--change .section2 .hotelList__item.active"
    ).removeClass("active");
    $(".checkbox-section.checkbox__form.visible").removeClass("visible");
  });
}

function getHotels() {
  fetch(
    `${document.location.origin}/api/hotels?lang=${$(
      ".lang_de.selected_e"
    ).val()}`
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      renderAllHotels(data);
      smartSearch(data);
    });
}

function smartSearch(data) {
  var availableTags = data.map((d) => {
    return {
      value: d.translations.title,
      title: d.translations.title,
      short_desc: d.translations.short_desc,
      desc: d.translations.desc,
      geo: d.geolocation,
      img: d.img != "" ? d.img : "",
    };
  });

  $("#tags")
    .autocomplete({
      minLength: 0,
      source: availableTags,
      focus: function (event, ui) {
        $("#tags").val(ui.item.title);
        return false;
      },
      select: function (event, ui) {
        $("#tags").val(ui.item.title);
        $("#tags-id").val(ui.item.value);
        $("#tags-description").html(ui.item.desc);

        return false;
      },
    })
    .autocomplete("instance")._renderItem = function (ul, item) {
    return $("<li>")
      .append(
        "<div>" +
          item.title +
          `<input hidden data-title='${item.title}' data-desc='${item.desc}' data-short-desc='${item.short_desc}' data-geo='${item.geo}' data-img='${item.img}'/>` +
          "</div>"
      )
      .appendTo(ul);
  };

  setNewLocationByClick();
}

function getCurrentData(gettedDate) {
  let today = new Date(gettedDate);

  let day = today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
  let month =
    today.getMonth() + 1 < 10
      ? `0${today.getMonth() + 1}`
      : today.getMonth() + 1;

  var date = day + "." + month + "." + today.getFullYear();
  var dataPickerDate = `${today.getFullYear()}-${month}-${day}`;
  getData(dataPickerDate);

  $(".timepicker__now").text(date);
}

function changeDay(gettedDate) {
  let dayCounter = 0;
  $(document).on("click", ".timepicker__next, .timepicker__prev", function (e) {
    let className = e.currentTarget.className;
    let date = new Date(gettedDate);
    let today = new Date();

    dayCounter < 0 && className == "timepicker__next"
      ? (dayCounter = 0)
      : dayCounter;
    className == "timepicker__next" ? ++dayCounter : --dayCounter;
    date.setDate(date.getDate() + dayCounter);

    dayCounter <= 14 && dayCounter >= 0 ? getCurrentData(date) : dayCounter;
  });
}

function create_custom_dropdowns() {
  $("select").each(function (i, select) {
    if (!$(this).next().hasClass("dropdown")) {
      $(this).after(
        '<div class="dropdown ' +
          ($(this).attr("class") || "") +
          '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>'
      );
      var dropdown = $(this).next();
      var options = $(select).find("option");
      var selected = $(this).find("option:selected");
      dropdown
        .find(".current")
        .html(selected.data("display-text") || selected.text());
      options.each(function (j, o) {
        var display = $(o).data("display-text") || "";
        dropdown
          .find("ul")
          .append(
            '<li class="option ' +
              ($(o).is(":selected") ? "selected" : "") +
              '" data-value="' +
              $(o).val() +
              '" data-display-text="' +
              display +
              '">' +
              $(o).text() +
              "</li>"
          );
      });
    }
  });
}

function showAdditionalServices(elem) {
  const service_to_hotel_pk = $("body").hasClass("book-admin")
    ? $(elem).eq(0).attr("data-id")
    : serviceID;
  const lang = $(".lang_de.selected_e").val();

  let getAdditionalServices = new Promise((resolve, reject) => {
    fetch(
      `${document.location.origin}/api/add_services/${service_to_hotel_pk}/?lang=${lang}`
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        resolve(data);
      });
  });

  getAdditionalServices.then((val) => {
    const container = $(".checkbox-section.checkbox__form.non-clickable div");

    container.html(
      val.map((v) => {
        return `
        <label class="label-trigger">
          <input type="checkbox" name="time">
          <h2><span class="checkbox__title">${v.translations.title}</span> + ₣<span id="${v.id}" class="currentLabelPrice">${v.price}</span></h2>
          <span class="checkmark"></span>
          <span class="choosed-label"></span>
        </label>`;
      })
    );

    $(".checkbox-section.checkbox__form.non-clickable")
      .addClass("visible")
      .removeClass("non-clickable");
  });
}

// Event listeners

// Open/close
$(document).on("click", ".dropdown", function (event) {
  $(".dropdown").not($(this)).removeClass("open");
  $(this).toggleClass("open");
  if ($(this).hasClass("open")) {
    $(this).find(".option").attr("tabindex", 0);
    $(this).find(".selected").focus();
  } else {
    $(this).find(".option").removeAttr("tabindex");
    $(this).focus();
  }
});
// Close when clicking outside
$(document).on("click", function (event) {
  if ($(event.target).closest(".dropdown").length === 0) {
    $(".dropdown").removeClass("open");
    $(".dropdown .option").removeAttr("tabindex");
  }
  event.stopPropagation();
});
// Option click
$(document).on("click", ".dropdown .option", function (event) {
  $(this).closest(".list").find(".selected").removeClass("selected");
  $(this).addClass("selected");
  var text = $(this).data("display-text") || $(this).text();
  $(this).closest(".dropdown").find(".current").text(text);
  $(this)
    .closest(".dropdown")
    .prev("select")
    .val($(this).data("value"))
    .trigger("change");
});

// Keyboard events
$(document).on("keydown", ".dropdown", function (event) {
  var focused_option = $(
    $(this).find(".list .option:focus")[0] ||
      $(this).find(".list .option.selected")[0]
  );
  // Space or Enter
  if (event.keyCode == 32 || event.keyCode == 13) {
    if ($(this).hasClass("open")) {
      focused_option.trigger("click");
    } else {
      $(this).trigger("click");
    }
    return false;
    // Down
  } else if (event.keyCode == 40) {
    if (!$(this).hasClass("open")) {
      $(this).trigger("click");
    } else {
      focused_option.next().focus();
    }
    return false;
    // Up
  } else if (event.keyCode == 38) {
    if (!$(this).hasClass("open")) {
      $(this).trigger("click");
    } else {
      var focused_option = $(
        $(this).find(".list .option:focus")[0] ||
          $(this).find(".list .option.selected")[0]
      );
      focused_option.prev().focus();
    }
    return false;
    // Esc
  } else if (event.keyCode == 27) {
    if ($(this).hasClass("open")) {
      $(this).trigger("click");
    }
    return false;
  }
});

function setCustomTime(data) {
  let obj = { hour: data[0].hour, minute: 0 };

  let interval = $("body").hasClass("book-admin")
    ? (parseInt(
        $(".cardsList__item.hotelList__item.active .cardText input").val()
      ) +
        15) /
      5
    : (period + 15) / 5;
  $(document).on(
    "click",
    ".timeline__picker-container .list ul li",
    function () {
      $('.generalSteps li:last-of-type').removeClass('finished');

      $(".timeline__snake div").removeClass("check");
      $(".timeline__snake div").removeClass("conflict");
      $(".button.noHover.next.next3.disabled").removeClass("disabled")
      
      let type = $(this).data("value");
      let value = parseInt($(this).text());

      obj[type] = value;
      let findedElem = $(
        'div[data-hour="' + obj.hour + '"][data-minute="' + obj.minute + '"]'
      );

      let customTime = {customTime: true, currentTime: obj}

      sessionStorage.setItem('customTime', JSON.stringify(customTime))
      // let siblingElem = findedElem.nextAll().slice(0, interval - 1);

      // findedElem.hasClass("locked")
      //   ? findedElem.addClass("conflict")
      //   : findedElem.addClass("check");
      // [...siblingElem].forEach((i) =>
      //   $(i).hasClass("locked")
      //     ? $(i).addClass("conflict")
      //     : $(i).addClass("check")
      // );

      // $(".timeline__snake div.conflict").length == 0
      //   ? $(".button.noHover.next.next3.disabled").removeClass("disabled")
      //   : $(".button.noHover.next.next3").addClass("disabled");
    }
  );
}

function setCustomTimeByClick() {
  let interval = $("body").hasClass("book-admin")
    ? (parseInt(
        $(".cardsList__item.hotelList__item.active .cardText input").val()
      ) +
        15) /
      5
    : (period + 15) / 5;
  $(document).on("click", ".timeline__snake div", function () {
    $('.generalSteps li:last-of-type').removeClass('finished');
    $(this).siblings().removeClass("check");
    $(this).siblings().removeClass("conflict");
    $(this).addClass("check");
    $(this).hasClass("locked") ? $(this).addClass("conflict") : null;
    $(this)
      .nextAll()
      .slice(0, interval - 1)
      .addClass("check");
    let elements = $(this)
      .nextAll()
      .slice(0, interval - 1);
    [...elements].forEach((i) =>
      $(i).hasClass("locked") ? $(i).addClass("conflict") : null
    );
    $(this).index() > $(".timeline__snake div").length - interval
      ? [
          [...elements].forEach((i) => $(i).addClass("conflict")),
          $(this).addClass("conflict"),
        ]
      : null;

    $(".timeline__snake div.conflict").length == 0
      ? $(".button.noHover.next.next3.disabled").removeClass("disabled")
      : $(".button.noHover.next.next3").addClass("disabled");
      sessionStorage.removeItem('customTime');
  });
}

function getData(day) {
  $(".loader").show();
  $(".timepicker__prev, .timepicker__next").addClass("locked");
  fetch(
    `${document.location.origin}/api/time/?id=${gettedSuperData.hotelID}&date=${day}`
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      renderSnake(data);
      renderHours(data);

      const promise = new Promise((resolve, reject) => {
        // generateDataInsideCustomSelect(data);
        resolve();
      });

      promise.then((result) => {
        setCustomTime(data);
        create_custom_dropdowns();
        $(".loader").hide();
        $(".timepicker__prev, .timepicker__next").removeClass("locked");
        checkLastItem(data);
      });
    });
}

function renderSnake(data) {
  let container = $(".timeline__snake");
  container.html(
    data.map((d) => {
      // if (d.minute) {
      return `<div data-hour="${d.hour}" class="${
        d.status == 1 ? "locked" : ""
      }"data-minute="${d.minute}"> </div>`;
      // }
    })
  );
}

function renderHours(data) {
  let container = $(".timeline__hours");

  container.html(
    data.map((d) => {
      if (d.minute == 0) {
        return `<div><p>${d.hour < 10 ? "0" + d.hour : d.hour}: 00</p></div>`;
      }
    })
  );
}

function openCustomTimePicker() {
  $(document).on(
    "click",
    ".timeline__header-custom, .timeline__picker-close",
    function () {
      $(".timeline__picker").toggleClass("visible");
    }
  );
}

function generateDataInsideCustomSelect(data) {
  const hourContainer = $("select.timeline__picker-hour");
  const minuteContainer = $("select.timeline__picker-minute");

  let hours = data.map((d) => {
    if (d.minute == 0) {
      return d.hour < 10
        ? `<option data-type="hour" value="hour"> 0${d.hour} </option>`
        : `<option data-type="hour" value="hour"> ${d.hour} </option>`;
    }
  });

  let minutes = data.map((d) => {
    return d.minute < 10
      ? `<option data-type="minute" value="minute"> 0${d.minute} </option>`
      : `<option data-type="minute" value="minute"> ${d.minute} </option>`;
  });

  hourContainer.html(hours);
  minuteContainer.html([...new Set(minutes)]);
}

function checkLastItem(data) {
  if (data[data.length - 1].minute == 0) {
    $(".timeline__hours div").last().remove();
    $(".timeline__snake div").last().remove();
  }
}

function setActiveAdditionalLabel() {
  $(document).on("click", ".label-trigger", function (e) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();
    $(this).find(".choosed-label").toggleClass("visible");
    $(this).find(".currentLabelPrice").toggleClass("choosen");
    $(this).find(".checkbox__title").toggleClass("choosen");
  });
}

function calculateData() {
  $(document).on("click", "#calculateButton, #generateSnake", function () {
    const cardPrice = $(".cardsList__item.hotelList__item.active")
      .find(".price")
      .data();
    const labels = $("body").hasClass("book-admin")
      ? $(".checkbox-section.checkbox__form.visible").find(
          ".currentLabelPrice.choosen"
        )
      : $(".currentLabelPrice.choosen");

    let gettedPriceFromLabel = [...labels].reduce((acc, l) => {
      return acc + parseInt($(l).text());
    }, 0);

    switch (gettedSuperData.userRole) {
      case "therapist":
        sessionStorage.setItem(
          "price",
          +cardPrice.price + +gettedPriceFromLabel
        );
        break;
      case "hotel_admin":
        sessionStorage.setItem("price", +currentPrice + +gettedPriceFromLabel);
        break;
      case "superuser":
        sessionStorage.setItem(
          "price",
          +cardPrice.price + +gettedPriceFromLabel
        );
        break;
      default:
        break;
    }

    // if(gettedSuperData.userRole !== 'therapist' && gettedSuperData.userRole !== 'hotel_admin') {
    //   sessionStorage.setItem(
    //     "price",
    //     $("body").hasClass("book-admin")
    //       ? parseInt(cardPrice.price) + gettedPriceFromLabel
    //       : gettedPriceFromLabel
    //   )
    // } else {
    //   sessionStorage.setItem(
    //     "price",
    //     $("body").hasClass("book-admin")
    //       ? parseInt(cardPrice.price) + gettedPriceFromLabel + currentPrice
    //       : gettedPriceFromLabel + currentPrice
    //   )
    // }
  });
}

function proceedToCheckout() {
  $(document).on("click", "#goToCheckout", function () {
    let time = $(".check").first().data();
    let title;
    let customTime = JSON.parse(sessionStorage.getItem('customTime'));

    if(customTime) {
      time = customTime.currentTime

    } else {
      time = $(".check").first().data();
    }
    
    if(gettedSuperData.userRole === 'hotel_admin') {
      $('.lang_de.selected_e').val() === 'en' ? title = serviseNameEng : title = serviseNameDe
    } else {
      title = $(".cardsList__item.hotelList__item.active")
      .find(".cardText h3")
      .text();
    }
    let adds = $(".checkbox__title.choosen");
    let addsTitles = [...adds].map((a) => $(a).text());

    $(".book3 .mainContent--change .section3 .note h5 span").text(
      `${sessionStorage.getItem("price")}₣`
    );
    $(".book3 .mainContent--change .section3 .note span")
      .first()
      .text(
        `${$(".timepicker__now").text()} at ${
          time.hour < 10 ? "0" + time.hour : time.hour
        }:${time.minute < 10 ? "0" + time.minute : time.minute}`
      );
    $("body").hasClass("book-admin")
      ? $(".sub__note").html(
          `${title} <span class="sub__note-adds">${
            addsTitles.length > 0 ? "+ " + addsTitles.join(', ') : ""
          }</span>`
        )
      : $(".sub__note").html(
          `${title} <span class="sub__note-adds">${
            addsTitles.length > 0
              ? `+ ${addsTitles.join()}`
              : ''
          }</span>`
        );
  });
}

function triggerValidation() {
  $(document).on("click", "#sendDataToAPI", function (e) {
    e.preventDefault();
    $('#validateForm').click();
  });
}

function sendDataToApi() {
    const str = $(".note span").first().text();
    const year = str.slice(6, 10);
    const month = str.slice(3, 5);
    const day = str.slice(0, 2);
    const time = str.slice(15);

    let customTime = sessionStorage.getItem('customTime')

    const sendedTime = `${year}-${month}-${day}T${time}`;
    const serviceID = $(".cardsList__item.hotelList__item.active")
      .find("input")
      .data();
    const serviceList = $(".currentLabelPrice.choosen");
    const serviceListIDs = [...serviceList].map((s) => {
      return parseInt($(s).attr("id"));
    });
    const userType = gettedSuperData.userRole;
    
    let obj = {
      book_data_time: sendedTime,
      hotel_id: gettedSuperData.hotelID,
      client_first_name: $(".client-name").val(),
      client_last_name: $(".client-surname").val(),
      client_room: $(".client-room").val(),
      client_email: $(".client-email").val(),
      comment: $(".client-comment").val(),
      add_service_list: serviceListIDs,
    };

    customTime ? obj.special_time = true : obj.status = 'active'


    $("body").hasClass("book-admin")
      ? (obj.service_id = serviceID.id)
      : (obj.service_id = gettedSuperData.serviceID);

    switch (userType) {
      case "therapisth":
        obj.therapist_id = gettedSuperData.userId;
        break;
      case "hotel_admin":
        obj.hotel_admin_id = gettedSuperData.userId;
        break;
      default:
        break;
    }

    fetch(`${document.location.origin}/api/orders/`, {
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": csrf,
      },
      method: "post",
      credentials: "same-origin",
      body: JSON.stringify(obj),
    }).then((response) => {
      return response.json()
    }).then((data) => sessionStorage.setItem('gettedIDFromAPI', data.id))

    $("#payment-checkbox").hasClass("active")
      ? $.fancybox.open({
          src: "#hidden-content",
          type: "inline",
          opts: {
            afterShow: function (instance, current) {
            },
          },
        })
      : [
          $(".sections.section3.widthWrapper").removeClass("visible"),

          $(".sections.section4.widthWrapper").addClass("visible"),
        ];

    $([document.documentElement, document.body]).animate(
      {
        scrollTop: $("#anchorToTheDatepicker").offset().top,
      },
      0
    );
  
}

function sendDataToPaymantService() {
  $(document).on("click", "#sendDataCardToAPI", function (e) {
    e.preventDefault();

    let order_id = sessionStorage.getItem('gettedIDFromAPI');

    // if(gettedSuperData.userRole === 'hotel_admin') {
    //   order_id = gettedSuperData.serviceID;
    // } else {
    //   let serviceID = $('.cardsList__item.hotelList__item.active').data()
    //   order_id = serviceID.liId;
    //   console.log(serviceID)
    // }
    let obj = {
      order_id: order_id,
      client_cart_number: $("#card__number").val(),
      exp_month: $("#card__month").val(),
      exp_year: $("#card__year").val(),
      client_cvv: $("#card__cvv").val(),
    };

    // obj.order_id = gettedSuperData.serviceID;

    fetch(`${document.location.origin}/api/create-payment-intent/`, {
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": csrf,
      },
      method: "post",
      credentials: "same-origin",
      body: JSON.stringify(obj),
    }).then((serverPromise) => {
      serverPromise
        .json()
        .then((j) => {
          if (j.status === "ok") {
            showPopupAfterSuccessPaymant(obj)
          } else {
            showPaymanErrors(j.errors);
          }
        })
        .catch((e) => {
          console.error(e);
        });
    });
    // .catch((e) => console.log(e));

    // fetch(`${document.location.origin}/api/create-payment-intent/`, {
    //   headers: {
    //     "Content-Type": "application/json",
    //     "X-CSRFToken": csrf,
    //   },
    //   method: "post",
    //   credentials: "same-origin",
    //   body: JSON.stringify(obj),
    // })
    //   .then((response) => {
    //     console.log(response);
    //     if (response.ok) {
    //       return response.json();
    //     } else {
    //       let data = response.json();
    //       console.log(data)
    //       throw new Error(response.json());
    //     }
    //   })

    //   .then(() => {
    //     fetch(
    //       `${document.location.origin}/api/orders/${gettedSuperData.serviceID}`,
    //       {
    //         headers: {
    //           "Content-Type": "application/json",
    //           "X-CSRFToken": csrf,
    //         },
    //         method: "put",
    //         credentials: "same-origin",
    //         body: JSON.stringify(obj),
    //       }
    //     ).then((response) =>
    //       response.ok
    //         ? [
    //             $.fancybox.close(),
    //             $(".sections.section3.widthWrapper").removeClass("visible"),
    //             $(".sections.section4.widthWrapper").addClass("visible"),
    //           ]
    //         : ""
    //     );
    //   })
    //   .catch((error) => {
    //     // console.log(error);
    //   });
  });
}

function setNewLocationByClick() {
  $(document).on("click", ".ui-menu-item", function () {
    let gettedData = $(this).find("input").data();

    let lat = gettedData.geo.split(",")[0];
    let lng = gettedData.geo.split(",")[1];
    let desc = gettedData.shortDesc;

    initMap({ lat: lat, lng: lng, desc });

    $(".cardContent__text span").text(gettedData.shortDesc);
    $(".cardContent__text h2").text(gettedData.title);
    $(".cardContent__text p").text(gettedData.desc);

    gettedData.img != ""
      ? $(".cardContent__pic > img").attr("src", gettedData.img)
      : "";
    $(".cardContentWrapper").show();
  });
}

function initMap(data) {
  var myCenter = new google.maps.LatLng(data.lat, data.lng);
  var mapProp = {
    center: myCenter,
    zoom: 13,
    // mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map"), mapProp);
  var marker = new google.maps.Marker({
    position: myCenter,
    title: data.desc,
  });
  marker.setMap(map);
  var infowindow = new google.maps.InfoWindow({
    content: data.desc,
  });
  infowindow.open(map, marker);
}
// google.maps.event.addDomListener(window, 'load', initMap({lat: 50.3268556, lng: 26.5684051}));

function renderAllHotels(data) {
  let locations = data.map((d) => {
    let lat = d.geolocation.split(",")[0];
    let lng = d.geolocation.split(",")[1];
    let desc = d.translations.short_desc;

    return { lat: lat, lng: lng, desc };
  });

  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 6,
    center: new google.maps.LatLng(50.4019514, 30.3926093),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  });

  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
      map: map,
    });

    google.maps.event.addListener(
      marker,
      "click",
      (function (marker, i) {
        return function () {
          infowindow.setContent(location[i].desc);
          infowindow.open(map, marker);
        };
      })(marker, i)
    );
  }
}

function filterBookingList() {
  const statusEnum = {
    Active: "active",
    Past: "past",
    "Awaiting confirmation": "awaiting_confirmation",
    "On contract": "",
    Cancelled: "",
    All: "default",
  };

  $(document).on("click", ".preTable .statusTrigger", function () {
    let queries = {
      hotel: "",
      status: "",
      therapist: "",
      hotel_admin: "",
      book_data_time: "",
    };

    let filterLink = "";
    let choosenFilterType = $(this).find("p").text();

    queries.status = statusEnum[choosenFilterType];
    sessionStorage.setItem("status", statusEnum[choosenFilterType]);
    let sessionFilter = sessionStorage.getItem("filter");
    let hotelID = sessionStorage.getItem("hotelID");
    let status = sessionStorage.getItem("status");
    let book_data_time = sessionStorage.getItem("book_data_time");

    switch (gettedSuperData.userRole) {
      case "hotel_admin":
        filterLink = `${
          document.location.origin
        }/api/orders?&status=${status}&hotel_admin=${
          gettedSuperData.userId
        }&book_data_time=${book_data_time}&hotel=${hotelID && hotelID !== 'undefined' ? hotelID : ""}`;
        sessionStorage.setItem("filter", filterLink);
        break;
      case "therapist":
        filterLink = `${
          document.location.origin
        }/api/orders?&status=${status}&therapist=${
          gettedSuperData.userId
        }&book_data_time=${book_data_time}&hotel=${hotelID ? hotelID : ""}`;
        sessionStorage.setItem("filter", filterLink);
        break;
      case "superuser":
        filterLink = `${
          document.location.origin
        }/api/orders?&status=${status}&superuser=${
          gettedSuperData.userId
        }&book_data_time=${book_data_time}&hotel=${hotelID && hotelID !== 'undefined' ? hotelID : ""}`;
        sessionStorage.setItem("filter", filterLink);
        break;
      default:
        break;
    }

    fetch(filterLink)
      .then((response) => {
        return response.json();
      })
      .then((data) => generateBookingList(data, 1));
  });
}

function getBookingListByState() {
  let queries = {
    status: "",
  };

  const statusEnum = {
    Active: "active",
    Past: "past",
    "Awaiting confirmation": "awaiting_confirmation",
    // "On contract": "",
    // Cancelled: "",
    All: "",
  };

  $(document).on("click", ".stateTrigger", function (e) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    let state = $(this).find("p").text();
    // let status = $(".preTable .active p").text();

    let filterLink = "";
    let hotel = $(".sub-steps.active").find("li.active").data();
    let book_data_time = sessionStorage.getItem("book_data_time");

    switch (state) {
      case "On contract":
        filterLink = `${
          document.location.origin
        }/api/orders?&pay_by_cart=false&hotel=${
          hotel ? hotel.id : ""
        }&book_data_time=${book_data_time}`;
        sessionStorage.setItem("filter", filterLink);

        break;
      case "Cancelled":
        filterLink = `${
          document.location.origin
        }/api/orders?&canceled=true&hotel=${
          hotel ? hotel.id : ""
        }&book_data_time=${book_data_time}`;
        sessionStorage.setItem("filter", filterLink);

        break;
      default:
        break;
    }

    fetch(filterLink)
      .then((response) => {
        return response.json();
      })
      .then((data) => generateBookingList(data, 1));
  });
}

function filterByData() {
  $(document).on("click", ".qs-square", function (e) {
    e.preventDefault();
    let day = $(this).text();
    let month = $(".qs-month").text();
    let year = $(".qs-year").text();
    let url;

    const monthEnum = {
      September: "09",
      October: "10",
      November: "11",
      December: "12",
      January: "01",
      February: "02",
      March: "03",
      April: "04",
      May: "05",
      June: "06",
      July: "07",
      August: "08",
    };

    sessionStorage.setItem(
      "book_data_time",
      `${year}-${monthEnum[month]}-${day < 10 ? `0${day}` : day}`
    );
    url = sessionStorage.getItem("filter");
    let oldURL = sessionStorage.getItem("filter");
    let newURL = oldURL.split("&book_data_time")[0];
    let status = sessionStorage.getItem("status");

    status === "" || status === null
      ? [
          (url = `${newURL}?book_data_time=${sessionStorage.getItem(
            "book_data_time"
          )}`),

        ]
      : (url = `${newURL}&book_data_time=${sessionStorage.getItem(
          "book_data_time"
        )}`);

    sessionStorage.setItem("filter", url);

    setTimeout(() => {
      if ($(".qs-active").length > 0) {
        fetch(url)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            generateBookingList(data, 1);
          });
      } else {
        let oldURL = sessionStorage.getItem("filter");
        let newURL;
        status === "" || status === null
          ? (newURL = oldURL.split("?book_data_time")[0])
          : (newURL = oldURL.split("&book_data_time")[0]);
        sessionStorage.setItem("filter", newURL);
        sessionStorage.setItem("book_data_time", "");
        fetch(newURL)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            generateBookingList(data, 1);
          });
      }
    }, 50);
  });
}

function relocateByClick() {
  $(document).on(
    "click",
    ".cardsList.slider .cardsList__item, .cardsList__item.chooseHotel, .cardsList__item.hotelList__item",
    function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();

      let href = $(this).find("a").attr("href");

      href ? document.location.href = href : '';
    }
  );
}

function simulateKeyPress(character) {
  jQuery.event.trigger({
    type: "keypress",
    which: character.charCodeAt(0),
  });
}

function shift_a_good() {
  //var evt = document.createEvent("KeyboardEvent");
  let evt = new KeyboardEvent("keydown", {
    shiftKey: true,
    key: "A",
    keyCode: 65,
  });
  document.dispatchEvent(evt);
}

function showHiddenList() {
  $(document).on("click", "#tags", function () {
    shift_a_good();
  });
}

function drillGetParametresToGeneratePdfButton() {
  $(document).on("click", "#renderToPdf", function (e) {
    e.preventDefault();
    let getParametres = sessionStorage.getItem("filter");
    let newParametres = getParametres.split("orders?&")[1];

    newParametres
      ? [
          (document.location.href = `${document.location.origin}/account/render_to_pdf/?${newParametres}`),
          console.log(
            (document.location.href = `${document.location.origin}/account/render_to_pdf/?${newParametres}`)
          ),
        ]
      : [
          (document.location.href = `${document.location.origin}/account/render_to_pdf/`),
          console.log(
            (document.location.href = `${document.location.origin}/account/render_to_pdf/?${newParametres}`)
          ),
        ];
  });
}

function clearDataFromLocalStorage() {
  sessionStorage.removeItem("status");
  sessionStorage.removeItem("book_data_time");
  sessionStorage.removeItem("filter");
  sessionStorage.removeItem("gettedIDFromAPI");
  sessionStorage.removeItem('customTime');
  sessionStorage.removeItem('date');
  sessionStorage.removeItem('hotelID');
}

function handlerForPromotionButton() {
  $(document).on("click", "#promotionBtn", function (e) {
    let id = $(this).data();

    if (gettedSuperData.userRole !== "hotel_admin") {
      $(".cardsList.hotelList").find(`[data-li-id=${id.id}]`)[0].click();
      $(".generalSteps > li:first-of-type")[0].click();
    } else {
      e.preventDefault();
      document.location.href = `${document.location.origin}/book/administration/`;
    }
  });
}

function showPaymanErrors(errors) {
  let container = $("#hidden-content .errors");
  let currentLanguage = $('.lang_de.selected_e').val()
  const currentDictionary = errorDictionary[currentLanguage]
  container.text(currentDictionary[errors]);
}

function showPopupAfterSuccessPaymant(obj) {
  let url = `${document.location.origin}/api/orders/${sessionStorage.getItem('gettedIDFromAPI')}`;

  // if(gettedSuperData.userRole === 'hotel_admin') {
  //   url = `${document.location.origin}/api/orders/${gettedSuperData.serviceID}`
  // } else {
  //   let serviceID = $('.cardsList__item.hotelList__item.active').data()
  //   url = `${document.location.origin}/api/orders/${serviceID}`
  // }
    fetch(url, {
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrf,
    },
    method: "put",
    credentials: "same-origin",
    body: JSON.stringify(obj),
  }).then((response) =>
    response.ok
      ? [
          $.fancybox.close(),
          $(".sections.section3.widthWrapper").removeClass("visible"),
          $(".sections.section4.widthWrapper").addClass("visible"),
        ]
      : ""
  );
}

function validateForm() {
  $(document).on('click', '#validateForm', function() {
    
  $("#userDataForm").validate({
    submitHandler: function() {
      sendDataToApi()
    },
    rules: {
      name: "required",
      surname: "required",
      email: {
        required: true,
        email: true
      },
      checkbox: "required"
    },
    messages: {
      name: "Please specify your name",
      surname: "Please specify your surname",
      checkbox: "You need to agree with the rules to continue working",
      email: {
        required: "We need your email address to contact you",
        email: "Your email address must be in the format of name@domain.com"
      }
    }
  });
  })
  
}

