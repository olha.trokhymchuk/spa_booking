from django.db import models
from django.utils.translation import gettext as _
from model_utils.models import TimeStampedModel
from hotel_app.models import HotelModel
from service_app.validators import validate_minutes
from spa_booking.constants import *
from spa_booking.validators import ImageValidator


class ServiceModel(TimeStampedModel):

    class Meta:
        db_table = 'services'
        verbose_name = _('Service')
        verbose_name_plural = _('Services')

    img = models.ImageField(verbose_name=_('Image'), upload_to='service/img', validators=[ImageValidator(size=256000, width=1000, height=1000)], null=True, blank=True)
    video = models.CharField(verbose_name=_("Video link"), max_length=255, null=True, blank=True)
    price = models.DecimalField(verbose_name=_('Price'), decimal_places=2, max_digits=10, null=True, blank=True)
    minutes = models.PositiveIntegerField(verbose_name=_('Duration of the procedure in minutes'),
                                          validators=[validate_minutes], null=True, blank=True)

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class ServiceTranslationModel(TimeStampedModel):
    class Meta:
        db_table = 'service_translation'
        verbose_name = _('Service translation')
        verbose_name_plural = _('Service translations')
        constraints = [
            models.UniqueConstraint(fields=['service', 'lang'],
                                    name='unique_service_lang_translation')
        ]

    service = models.ForeignKey(to="ServiceModel", on_delete=models.CASCADE, related_name='translations',
                                verbose_name=_("Service"),
                                null=False, blank=False,
                                )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)
    short_desc = models.TextField(verbose_name=_("Short description"), null=False, blank=False)
    desc = models.TextField(verbose_name=_('Description'), null=False, blank=False)


class AdditionalServiceModel(TimeStampedModel):

    class Meta:
        db_table = 'additional_services'
        verbose_name = _('Additional service')
        verbose_name_plural = _('Additional services')

    price = models.DecimalField(verbose_name=_('Price'), decimal_places=2, max_digits=10, null=True, blank=True)

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class AdditionalServiceTranslationModel(TimeStampedModel):

    class Meta:
        db_table = 'additional_service_translation'
        verbose_name = _('Additional service translation')
        verbose_name_plural = _('Additional service translations')
        constraints = [
            models.UniqueConstraint(fields=['additional_service', 'lang'],
                                    name='unique_additional_service_lang_translation')
        ]

    additional_service = models.ForeignKey(to="AdditionalServiceModel", on_delete=models.CASCADE, related_name='translations',
                                verbose_name=_("Additional service"),
                                null=False, blank=False,
                                )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)
    desc = models.TextField(verbose_name=_('Description'), null=False, blank=False)


class ServiceToHotelModel(TimeStampedModel):

    class Meta:
        db_table = 'services_to_hotel'
        verbose_name = _('Relation of service with the hotel')
        verbose_name_plural = _('Relations of services with the hotel')
        constraints = [
            models.UniqueConstraint(fields=['service', 'hotel'],
                                    name='unique_service_to_hotel')
        ]

    service = models.ForeignKey(to="ServiceModel", on_delete=models.CASCADE, related_name='hotels',
                                verbose_name=_("Service"),
                                null=False, blank=False,
                                )
    hotel = models.ForeignKey(to=HotelModel, on_delete=models.CASCADE, related_name='services',
                              verbose_name=_("Hotel"),
                              null=False, blank=False,
                              )
    promotional_service = models.BooleanField('Promotional service', default=False)
    price = models.DecimalField(verbose_name=_('Price'), decimal_places=2, max_digits=10, null=False, blank=False)
    minutes = models.PositiveIntegerField(verbose_name=_('Duration of the procedure in minutes'), validators=[validate_minutes], null=False, blank=False)

    def __str__(self):
        return f'{self.hotel} - {self.service}'


class AdditionalServiceToServiceInHotelModel(TimeStampedModel):

    class Meta:
        db_table = 'additional_services_to_service_in_hotel'
        verbose_name = _('Relation of additional services with the main ones')
        verbose_name_plural = _('Relations of additional services with the main ones')
        constraints = [
            models.UniqueConstraint(fields=['additional_service', 'service'], name='unique_additional_service_to_service')
        ]

    additional_service = models.ForeignKey(to="AdditionalServiceModel", on_delete=models.CASCADE,
                                           related_name='service',
                                           verbose_name=_("Additional service"),
                                           null=False, blank=False,
                                           )
    service = models.ForeignKey(to="ServiceToHotelModel", on_delete=models.CASCADE, related_name='additional_services',
                                verbose_name=_("Service in hotel"),
                                null=False, blank=False,
                                )
    price = models.DecimalField(verbose_name=_('Price'), decimal_places=2, max_digits=10, null=False, blank=False)
