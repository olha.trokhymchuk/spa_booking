import stripe
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views import generic

from info_app.models import ContactModel
from spa_booking.helper import user_type, get_footer_info
from .models import *

stripe.api_key = settings.STRIPE_SECRET_KEY


@login_required
def lk(request):
    redirect_to = reverse(viewname='administration_booking_list')
    return HttpResponseRedirect(redirect_to)


class AdministrationBookingList(generic.ListView):
    model = ServiceToHotelModel
    template_name = 'service_app/book2.html'
    context_object_name = 'service_to_hotel_list'

    def get_context_data(self, **kwargs):
        context = super(AdministrationBookingList, self).get_context_data(**kwargs)
        hotels_list = HotelModel.objects.all()
        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'user_type': user_type(self.request.user),
             'hotels_list': hotels_list}
        )
        return context

    def get_queryset(self):
        queryset = super(AdministrationBookingList, self).get_queryset()
        if user_type(self.request.user).get('is_administrator'):
            queryset = queryset.filter(hotel__administrators=self.request.user).all()
        if user_type(self.request.user).get('is_therapist'):
            queryset = queryset.filter(hotel__therapists=self.request.user).all()
        return queryset


class BookingService(generic.ListView):
    model = ServiceToHotelModel
    template_name = 'service_app/book3.html'
    context_object_name = 'service_to_hotel_list'

    def get_context_data(self, **kwargs):
        context = super(BookingService, self).get_context_data(**kwargs)
        service = get_object_or_404(ServiceToHotelModel, id=self.kwargs['service_id'])
        promotional_service = ServiceToHotelModel.objects.filter(promotional_service=True,
                                                                 hotel=user_type(self.request.user).get('user').hotel).first()
        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'user_type': user_type(self.request.user),
             'booking_service': service,
             'promotional_service': promotional_service,
             'STRIPE_SECRET_KEY': settings.STRIPE_SECRET_KEY}
        )
        return context

    def get_queryset(self):
        queryset = super(BookingService, self).get_queryset()
        user_info = user_type(self.request.user)
        if user_info.get('is_administrator') or user_info.get('is_therapist'):
            queryset = queryset.filter(hotel=user_info.get('user').hotel).all()
        return queryset


class BookingAdmin(generic.ListView):
    model = ServiceToHotelModel
    template_name = 'service_app/book-admin.html'
    context_object_name = 'services'

    def get_context_data(self, **kwargs):
        context = super(BookingAdmin, self).get_context_data(**kwargs)
        service_to_hotel = ServiceToHotelModel.objects.filter(hotel_id=self.kwargs['hotel_id'])
        promotional_service = ServiceToHotelModel.objects.filter(promotional_service=True,
                                                                 hotel_id=self.kwargs['hotel_id']).first()
        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'service_to_hotel': service_to_hotel,
             'user_type': user_type(self.request.user),
             'promotional_service': promotional_service}
        )
        return context





