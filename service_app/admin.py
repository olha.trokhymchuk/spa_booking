from django.utils.translation import gettext as _
from django.contrib import admin
from .models import *
from spa_booking.constants import *


class ServiceTranslationInLineAdminModel(admin.StackedInline):
    model = ServiceTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class ServiceAdminModel(admin.ModelAdmin):
    search_fields = ['translations__title']
    list_display = ['id', 'title_en', 'title_de']
    list_filter = ['hotels__hotel']

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    inlines = [
        ServiceTranslationInLineAdminModel
    ]


class AdditionalServiceTranslationInLineAdminModel(admin.StackedInline):
    model = AdditionalServiceTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class AdditionalServiceAdminModel(admin.ModelAdmin):
    search_fields = ['translations__title']
    list_display = ['id', 'title_en', 'title_de']

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    inlines = [
        AdditionalServiceTranslationInLineAdminModel
    ]


class AdditionalServiceToServiceInHotelModelInLineAdminModel(admin.TabularInline):
    model = AdditionalServiceToServiceInHotelModel


class ServiceToHotelAdminModel(admin.ModelAdmin):
    list_display = ['service', 'hotel', 'price', 'minutes', 'qty_add_service']

    def qty_add_service(self, obj):
        return obj.additional_services.count()

    qty_add_service.short_description = _('COUNT OF ADD SERVICE')

    list_filter = ['hotel', 'service']
    search_fields = ['hotel', 'service']

    fieldsets = (
        (_('Hotel'), {
            'fields': ('hotel',)
        }),
        (_('Service'), {
            'fields': ('service', 'promotional_service', 'price', 'minutes')
        }),
    )

    inlines = [
        AdditionalServiceToServiceInHotelModelInLineAdminModel
    ]


admin.site.register(ServiceModel, ServiceAdminModel)
admin.site.register(AdditionalServiceModel, AdditionalServiceAdminModel)
admin.site.register(ServiceToHotelModel, ServiceToHotelAdminModel)
