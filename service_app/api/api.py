import json

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from order_app.models import ProfilePay, OrderModel
import stripe

stripe.api_key = settings.STRIPE_SECRET_KEY


@csrf_exempt
def create_payment(request):
    if request.method == 'POST':
        if request.body:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            print('body : ', body)
            if body['client_cart_number'] and body['exp_month'] and body['exp_year'] \
                and body['client_cvv'] and body['order_id']:

                get_order = OrderModel.objects.get(pk=body['order_id'])
                try:
                    payment_method = stripe.PaymentMethod.create(
                        type="card",
                        card={
                            "number": body['client_cart_number'],
                            "exp_month": body['exp_month'],
                            "exp_year": body['exp_year'],
                            "cvc": body['client_cvv'],
                        },
                    )
                    # Create a PaymentIntent with the order amount and currency
                    stripe.PaymentIntent.create(
                        amount=int(get_order.total_price) * 10,
                        currency='chf',
                        payment_method=payment_method['id'],
                    )
                    return JsonResponse({'status': 'ok'}, status=200)
                except stripe.error.CardError as e:
                    print('Status is: %s' % e.http_status)
                    print('Code is: %s' % e.code)
                    return JsonResponse({'status': e.http_status, 'errors': e.code}, status=404)
            return JsonResponse({'errors': 'elements of card not found'}, status=404)
        return JsonResponse({'errors': 'request date not found'}, status=404)
    return JsonResponse(data={'errors': 'method not allowed'}, status=404)


def payment_intent_confirm(request):
    if request.method == 'POST':
        if request.body:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            print('body : ', body)
            if body['order_id']:
                get_order = OrderModel.objects.get(pk=body['order_id'])
                try:
                    stripe.PaymentIntent.confirm(
                        get_order.pay_order.pay_id,
                        payment_method=get_order.pay_order.payment_method_id,
                    )
                    get_profile_pey = ProfilePay.objects.get(pay_id=get_order.pay_order.first().pay_id)
                    get_profile_pey.status_pay_success = True
                    get_profile_pey.save()
                    return JsonResponse({'error': '0'})
                except stripe.error.CardError as e:
                    print('Status is: %s' % e.http_status)
                    print('Code is: %s' % e.code)
                    return JsonResponse({'status': e.http_status, 'code': e.code}, status=404)
            return JsonResponse({'errors': 'request order id not found'}, status=404)
        return JsonResponse({'errors': 'request date not found'}, status=404)
    return JsonResponse(data={'errors': 'method not allowed'}, status=404)


def cancel_payment(request):
    if request.method == 'POST':
        if request.body:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            if body['order_id']:
                try:
                    get_order = OrderModel.objects.get(pk=body['order_id'])
                    stripe.PaymentIntent.cancel(
                        get_order.pay_order.first().pay_id,
                    )
                    return JsonResponse({'error': '0'})
                except stripe.error.CardError as e:
                    print('Status is: %s' % e.http_status)
                    print('Code is: %s' % e.code)
                    return JsonResponse({'status': e.http_status, 'code': e.code}, status=404)
            return JsonResponse({'errors': 'request order id not found'}, status=404)
        return JsonResponse({'errors': 'request date not found'}, status=404)
    return JsonResponse(data={'errors': 'method not allowed'}, status=404)
