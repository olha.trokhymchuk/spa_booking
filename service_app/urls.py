from django.urls import path

from service_app.views import lk, AdministrationBookingList, BookingService, BookingAdmin

urlpatterns = [
    path('', lk, name="book"),
    path('administration/', AdministrationBookingList.as_view(), name="administration_booking_list"),
    path('service/<int:service_id>/', BookingService.as_view(), name="service"),
    path('book-admin/<int:hotel_id>/', BookingAdmin.as_view(), name="book_admin"),
]
