# Generated by Django 3.2.6 on 2021-09-16 09:15

from django.db import migrations, models
import spa_booking.validators


class Migration(migrations.Migration):

    dependencies = [
        ('service_app', '0006_auto_20210910_0731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicemodel',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='service/img', validators=[spa_booking.validators.ImageValidator(height=1000, size=256000, width=1000)], verbose_name='Image'),
        ),
    ]
