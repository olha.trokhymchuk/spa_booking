from django.urls import path

from .api.api import create_payment, payment_intent_confirm, cancel_payment
from .api_views import *

urlpatterns = [
    path('service/<pk>/', ServiceRetrieveAPIView.as_view()),
    path('add_service/<pk>/', AdditionalServiceRetrieveAPIView.as_view()),
    path('add_services/<service_to_hotel_pk>/', AdditionalServiceListAPIView.as_view()),
    path("create-payment-intent/", create_payment, name="create-payment-intent"),
    path("payment-intent-confirm/", payment_intent_confirm, name="payment-intent-confirm"),
    path("cancel-payment/", cancel_payment, name="cancel-payment"),
]
