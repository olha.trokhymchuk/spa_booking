from django.core.exceptions import ValidationError
from spa_booking.constants import MINUTE_STEP
from django.utils.translation import gettext as _


def validate_minutes(value):
    if value % MINUTE_STEP != 0:
        raise ValidationError(_(f"Value should be a multiple of {MINUTE_STEP} minutes"))
