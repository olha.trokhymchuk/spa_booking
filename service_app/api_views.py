from rest_framework import generics
from rest_framework.response import Response

from spa_booking.custom_api_exeptions import Http400
from .helper import list_of_add_service_with_translation
from .serializers import *
from .models import *


class ServiceRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ServiceSerializer
    queryset = ServiceModel.objects.all()


class AdditionalServiceRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = AdditionalServiceSerializer
    queryset = AdditionalServiceModel.objects.all()


class AdditionalServiceListAPIView(generics.ListAPIView):
    def list(self, request, *args, **kwargs):
        if "lang" not in self.request.query_params:
            raise Http400(detail="lang - required field.")
        lang = self.request.query_params.get('lang')
        queryset = AdditionalServiceToServiceInHotelModel.objects.filter(service=self.kwargs.get('service_to_hotel_pk'))
        return Response(list_of_add_service_with_translation(queryset, lang))
