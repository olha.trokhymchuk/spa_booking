def list_of_add_service_with_translation(add_services, lang):
    add_service_resp = list()
    for add_service in add_services:
        translation = add_service.additional_service.translations.filter(lang__iregex=lang).first()
        add_service_resp += [
            {
                'id': add_service.id,
                "price": add_service.price,
                'translations': {
                    'lang': translation.lang,
                    'title': translation.title,
                    'desc': translation.desc,

                } if translation else {},
            }
        ]
    return add_service_resp
