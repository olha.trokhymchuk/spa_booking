from rest_framework import serializers
from .models import *


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceModel
        fields = ('id', 'price', 'minutes')


class AdditionalServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalServiceModel
        fields = ('id', 'price', )
