from django import template

register = template.Library()


@register.filter
def get_title(obj, lang):
    return obj.translations.filter(lang__iregex=lang).first().title \
        if obj.translations.filter(lang__iregex=lang).exists() else "-"


@register.filter
def get_short_desc(obj, lang):
    return obj.translations.filter(lang__iregex=lang).first().short_desc \
        if obj.translations.filter(lang__iregex=lang).exists() else "-"


@register.filter
def get_description(obj, lang):
    return obj.translations.filter(lang__iregex=lang).first().desc \
        if obj.translations.filter(lang__iregex=lang).exists() else "-"


register.filter('get_title', get_title)
register.filter('get_short_desc', get_short_desc)
register.filter('get_description', get_description)
