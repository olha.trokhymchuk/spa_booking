from django.urls import path

from info_app.views import HomeInformation, ContactUs, CustomLoginView

urlpatterns = [
    path('', HomeInformation.as_view(), name="home"),
    path('contact_us/', ContactUs.as_view(), name="contact_us"),
    path('login/', CustomLoginView.as_view(), name="login"),

]
