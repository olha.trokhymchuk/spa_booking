from django.contrib import admin
from .models import *
from django.utils.translation import gettext as _


class InformationPageTranslationInLineaAdminModel(admin.StackedInline):
    model = InformationTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class ServiceInfoTranslationInLineaAdminModel(admin.StackedInline):
    model = ServiceInfoTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class FooterInfoTranslationInLineaAdminModel(admin.StackedInline):
    model = FooterInfoTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class InformationPageAdminModel(admin.ModelAdmin):
    search_fields = ['translations__title']
    list_display = ['title_en', 'title_de', 'img']

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    inlines = [
        InformationPageTranslationInLineaAdminModel
    ]


class ServiceInfoAdminModel(admin.ModelAdmin):
    search_fields = ['translations__title']
    list_display = ['title_en', 'title_de', 'img']

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    inlines = [
        ServiceInfoTranslationInLineaAdminModel
    ]


class FooterInfoAdminModel(admin.ModelAdmin):
    search_fields = ['translations__title']
    list_display = ['title_en', 'title_de', 'img',]

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    inlines = [
        FooterInfoTranslationInLineaAdminModel
    ]


class ContactAdminModel(admin.ModelAdmin):
    list_display = ['id', 'img', 'social_networks_urls']


admin.site.register(InformationPageModel, InformationPageAdminModel)
admin.site.register(ServiceInfoModel, ServiceInfoAdminModel)
admin.site.register(FooterInfoModel, FooterInfoAdminModel)
admin.site.register(ContactModel, ContactAdminModel)


