# Generated by Django 3.2.6 on 2021-09-08 07:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('info_app', '0004_auto_20210825_0751'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmodel',
            name='social_networks_urls',
            field=models.CharField(default='', max_length=300, verbose_name='Social networks urls'),
        ),
        migrations.AlterField(
            model_name='contactmodel',
            name='img',
            field=models.FileField(blank=True, null=True, upload_to='contact/img', verbose_name='Contact photo'),
        ),
        migrations.DeleteModel(
            name='ContactTranslationModel',
        ),
    ]
