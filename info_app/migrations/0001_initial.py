# Generated by Django 3.2.6 on 2021-08-17 07:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, null=True, upload_to='contact/img', verbose_name='Contact photo')),
            ],
            options={
                'verbose_name': 'Contact',
                'verbose_name_plural': 'Contacts',
                'db_table': 'contact',
            },
        ),
        migrations.CreateModel(
            name='FooterInfoModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, null=True, upload_to='footer/img', verbose_name='Footer photo')),
            ],
            options={
                'verbose_name': 'Footer information',
                'verbose_name_plural': 'Footer information',
                'db_table': 'footer_info',
            },
        ),
        migrations.CreateModel(
            name='InformationPageModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, null=True, upload_to='info/img', verbose_name='Information photo')),
            ],
            options={
                'verbose_name': 'Information page',
                'verbose_name_plural': 'Information pages',
                'db_table': 'information_page',
            },
        ),
        migrations.CreateModel(
            name='ServiceInfoModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, null=True, upload_to='service/img', verbose_name='Service photo')),
                ('urls', models.CharField(default='', max_length=255, verbose_name='Service info urls')),
            ],
            options={
                'verbose_name': 'Service',
                'verbose_name_plural': 'Services',
                'db_table': 'service_info',
            },
        ),
        migrations.CreateModel(
            name='ServiceInfoTranslationModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(choices=[('EN', 'EN'), ('DE', 'DE')], max_length=2, verbose_name='Language')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('desc', models.TextField(verbose_name='Description')),
                ('service_information', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='service_info_translations', to='info_app.serviceinfomodel', verbose_name='Service information')),
            ],
            options={
                'verbose_name': 'Service information translation',
                'verbose_name_plural': 'Service information translation',
                'db_table': 'service_info_translation',
            },
        ),
        migrations.CreateModel(
            name='InformationTranslationModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(choices=[('EN', 'EN'), ('DE', 'DE')], max_length=2, verbose_name='Language')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('information', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='info_translations', to='info_app.informationpagemodel', verbose_name='Information')),
            ],
            options={
                'verbose_name': 'Information translation',
                'verbose_name_plural': 'Information translation',
                'db_table': 'information_translation',
            },
        ),
        migrations.CreateModel(
            name='FooterInfoTranslationModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(choices=[('EN', 'EN'), ('DE', 'DE')], max_length=2, verbose_name='Language')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('desc', models.TextField(verbose_name='Description')),
                ('footer_information', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='footer_info_translations', to='info_app.footerinfomodel', verbose_name='Footer information')),
            ],
            options={
                'verbose_name': 'Footer information translation',
                'verbose_name_plural': 'Footer information translation',
                'db_table': 'footer_info_translation',
            },
        ),
        migrations.CreateModel(
            name='ContactTranslationModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang', models.CharField(choices=[('EN', 'EN'), ('DE', 'DE')], max_length=2, verbose_name='Language')),
                ('company_name', models.CharField(default='', max_length=255, verbose_name='Company name')),
                ('contact_for_work', models.TextField(default='', verbose_name='Contact for work')),
                ('social_networks_urls', models.CharField(default='', max_length=255, verbose_name='Social networks urls')),
                ('contact', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contact_translations', to='info_app.contactmodel', verbose_name='Contact')),
            ],
            options={
                'verbose_name': 'Contact translation',
                'verbose_name_plural': 'Contacts translation',
                'db_table': 'contact_translation',
            },
        ),
        migrations.AddConstraint(
            model_name='serviceinfotranslationmodel',
            constraint=models.UniqueConstraint(fields=('service_information', 'lang'), name='unique_service_info_lang_translation'),
        ),
        migrations.AddConstraint(
            model_name='informationtranslationmodel',
            constraint=models.UniqueConstraint(fields=('information', 'lang'), name='unique_info_lang_translation'),
        ),
        migrations.AddConstraint(
            model_name='footerinfotranslationmodel',
            constraint=models.UniqueConstraint(fields=('footer_information', 'lang'), name='unique_footer_info_lang_translation'),
        ),
        migrations.AddConstraint(
            model_name='contacttranslationmodel',
            constraint=models.UniqueConstraint(fields=('contact', 'lang'), name='unique_contact_lang_translation'),
        ),
    ]
