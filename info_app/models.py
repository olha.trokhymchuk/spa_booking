from django.db import models
from django.utils.translation import gettext as _

from spa_booking.constants import *
from spa_booking.validators import ImageValidator


class InformationPageModel(models.Model):

    class Meta:
        db_table = 'information_page'
        verbose_name = _('Information page')
        verbose_name_plural = _('Information pages')

    img = models.ImageField(verbose_name=_('Information photo'), upload_to="info/img", validators=[ImageValidator(size=256000, width=1000, height=1000)], null=True, blank=True)

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class InformationTranslationModel(models.Model):
    class Meta:
        db_table = 'information_translation'
        verbose_name = _('Information translation')
        verbose_name_plural = _('Information translation')
        constraints = [
            models.UniqueConstraint(fields=['information', 'lang'],
                                    name='unique_info_lang_translation')
        ]

    information = models.ForeignKey(to="InformationPageModel", on_delete=models.CASCADE,
                                    related_name='translations',
                                    verbose_name=_("Information"),
                                    null=False, blank=False,
                                    )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)


class ServiceInfoModel(models.Model):

    class Meta:
        db_table = 'service_info'
        verbose_name = _('Service Info')
        verbose_name_plural = _('Services Info')

    CHOICES_URL = (
        ('home_url', 'home'),
        ('contact_url', 'contact_us'),
    )

    CHOICES_POSITION = (
        ('left_block', 'left'),
        ('centre_block', 'centre'),
        ('right_block', 'right'),
    )

    img = models.ImageField(verbose_name=_('Service photo'), validators=[ImageValidator(size=256000, width=1000, height=1000)], upload_to="service_info/img", null=True, blank=True)
    urls = models.CharField(verbose_name=_("Service info urls"), max_length=255, null=False,
                            blank=False, choices=CHOICES_URL, default='')
    position_block = models.CharField(verbose_name=_("Service info position block"), max_length=255, null=True,
                                      blank=True, choices=CHOICES_POSITION, default='')

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class ServiceInfoTranslationModel(models.Model):
    class Meta:
        db_table = 'service_info_translation'
        verbose_name = _('Service information translation')
        verbose_name_plural = _('Service information translation')
        constraints = [
            models.UniqueConstraint(fields=['service_information', 'lang'],
                                    name='unique_service_info_lang_translation')
        ]

    service_information = models.ForeignKey(to="ServiceInfoModel", on_delete=models.CASCADE,
                                            related_name='translations',
                                            verbose_name=_("Service information"),
                                            null=False, blank=False,
                                            )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)
    desc = models.TextField(verbose_name=_('Description'), null=False, blank=False)


class FooterInfoModel(models.Model):

    class Meta:
        db_table = 'footer_info'
        verbose_name = _('Footer information')
        verbose_name_plural = _('Footer information')

    img = models.ImageField(verbose_name=_('Footer photo'), upload_to="footer/img", validators=[ImageValidator(size=256000, width=1920, height=1080)], null=True, blank=True)

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class FooterInfoTranslationModel(models.Model):
    class Meta:
        db_table = 'footer_info_translation'
        verbose_name = _('Footer information translation')
        verbose_name_plural = _('Footer information translation')
        constraints = [
            models.UniqueConstraint(fields=['footer_information', 'lang'],
                                    name='unique_footer_info_lang_translation')
        ]

    footer_information = models.ForeignKey(to="FooterInfoModel", on_delete=models.CASCADE,
                                           related_name='translations',
                                           verbose_name=_("Footer information"),
                                           null=False, blank=False,
                                           )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)
    desc = models.TextField(verbose_name=_('Description'), null=False, blank=False)


class ContactModel(models.Model):
    class Meta:
        db_table = 'contact'
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')

    img = models.FileField(verbose_name=_('Contact photo'), upload_to="contact/img", null=True, blank=True)
    social_networks_urls = models.CharField(verbose_name=_("Social networks urls"), max_length=300, null=False,
                                                 blank=False, default='')

    # def __str__(self):
    #     return f'{self.contact_translations.get(lang=EN).company_name if self.contact_translations.filter(lang=EN) else "-"} / ' \
    #            f'{self.contact_translations.get(lang=DE).company_name if self.contact_translations.filter(lang=DE) else "-"}'

#
# class ContactTranslationModel(models.Model):
#     class Meta:
#         db_table = 'contact_translation'
#         verbose_name = _('Contact translation')
#         verbose_name_plural = _('Contacts translation')
#         constraints = [
#             models.UniqueConstraint(fields=['contact', 'lang'],
#                                     name='unique_contact_lang_translation')
#         ]
#
#     contact = models.ForeignKey(to="ContactModel", on_delete=models.CASCADE, related_name='contact_translations',
#                                 verbose_name=_("Contact"),
#                                 null=False, blank=False,
#                                 )
#     lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
#     company_name = models.CharField(verbose_name=_("Company name"), max_length=255, null=False, blank=False, default='')
#     contact_for_work = models.TextField(verbose_name=_("Contact for work"), null=False, blank=False, default='')
#     social_networks_urls = models.CharField(verbose_name=_("Social networks urls"), max_length=255, null=False,
#                                             blank=False, default='')
