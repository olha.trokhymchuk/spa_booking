from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.contrib.messages import get_messages
from django.shortcuts import redirect
from django.urls import reverse
from django.views import generic
from spa_booking.helper import get_footer_info
from .models import *
from django.utils.translation import get_language


class HomeInformation(generic.ListView):
    model = InformationPageModel
    template_name = 'info_app/home.html'
    context_object_name = 'information_list'

    def get_context_data(self, **kwargs):
        context = super(HomeInformation, self).get_context_data(**kwargs)
        try:
            service_centre_block = ServiceInfoModel.objects.get(position_block='centre_block')
            service_left_block = ServiceInfoModel.objects.get(position_block='left_block')
            service_right_block = ServiceInfoModel.objects.get(position_block='right_block')
        except ServiceInfoModel.DoesNotExist:
            return None

        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'service_centre_block': service_centre_block,
             'service_left_block': service_left_block,
             'service_right_block': service_right_block,
             }
        )
        return context

    def get_queryset(self):
        return InformationPageModel.objects.first()


class ContactUs(generic.ListView):
    model = InformationPageModel
    template_name = 'info_app/contactUs.html'
    context_object_name = 'contacts_list'

    def get_context_data(self, **kwargs):
        context = super(ContactUs, self).get_context_data(**kwargs)
        context.update(
            {'footer_info': get_footer_info(),
             'contacts': ContactModel.objects.all(),
             'form': AuthenticationForm,
             'errors': get_messages(self.request)}
        )
        return context


class CustomLoginView(LoginView):
    template_name = 'info_app/forms_html/login_form.html'

    def get_success_url(self):
        return reverse("book")

    def form_invalid(self, form):
        errors = []
        error_data = form.errors.as_data()
        for field in error_data:
            for error_class in error_data.get(field):
                for error in error_class:
                    errors.append(error)
        messages.error(request=self.request, message=''.join(errors))
        return redirect(reverse(viewname='contact_us'))
