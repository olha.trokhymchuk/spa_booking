from django.contrib import admin

from django.utils.translation import gettext as _
from django.contrib import admin
from .models import *
from spa_booking.constants import *


class OrderAdminModel(admin.ModelAdmin):
    search_fields = ['id', 'client_first_name', 'client_last_name', 'client_email',
                     'therapist__first_name', "therapist__last_name"]
    list_filter = ['hotel', 'service', 'hotel_admin', 'therapist', 'status', 'canceled', 'pay_by_cart']
    list_display = ['id', 'hotel', 'service', 'book_data_time', 'therapist', 'special_time', 'status', 'canceled', 'pay_by_cart']

    fieldsets = (
        (_('Hotel'), {
            'fields': ('hotel', 'hotel_admin', 'therapist')
        }),
        (_('Service'), {
            'fields': ('service', 'service_json', 'additional_service_json')
        }),
        (_('Client data'), {
            'fields': ('client_first_name', 'client_last_name', 'client_room', 'client_email')
        }),
        (_('Client cart'), {
            'fields': ('client_cart_number', 'exp_month', 'exp_year', 'client_cvv')
        }),
        (_('Detail'), {
            'fields': ('book_data_time', 'special_time', 'status', 'canceled', 'pay_by_cart', 'total_price', 'comment')
        }),
    )


admin.site.register(OrderModel, OrderAdminModel)

