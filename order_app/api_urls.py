from django.urls import path, re_path
from .api_view import *

urlpatterns = [
    path('time/', TimeListApiListView.as_view()),
    path('orders/', OrdersListApiView.as_view()),
    path('orders/<pk>', OrdersListApiView.as_view()),
    path('therapists/', TherapistsListView.as_view()),
]
