from datetime import datetime, date, timedelta
from decimal import Decimal

from service_app.models import ServiceToHotelModel
from spa_booking.constants import MINUTE_STEP


def create_dict_time(time_list):
    data = []
    for time in time_list:
        data.append({
            'hour': time[0].hour,
            'minute': time[0].minute,
            'second': time[0].second,
            'status': time[1]

        })
    return data


def get_list_for_time_piker(hotel, order_list, therapist_qty, date_day):
    hotel_work_in_minutes = (hotel.spa_service_work_time_to.hour * 60 + hotel.spa_service_work_time_to.minute) - \
                            (hotel.spa_service_work_time_from.hour * 60 + hotel.spa_service_work_time_from.minute)
    _date = datetime.strptime(date_day, '%Y-%m-%d').date()
    time_list = [[(datetime.combine(_date, hotel.spa_service_work_time_from) + timedelta(minutes=index * MINUTE_STEP)).time(), 0]
                 for index in range(int((hotel_work_in_minutes / MINUTE_STEP)+1))]

    if not order_list:
        return create_dict_time(time_list)

    for item in time_list:
        orders = order_list.filter(book_data_time__lte=datetime.combine(_date, item[0]))
        qty = 0
        for order in orders:
            service_minutes = order.service.minutes if order.service else order.service_json.get('minutes')
            if order.book_data_time.time() >= (datetime.combine(_date, item[0]) - timedelta(minutes=service_minutes+15)).time():
                qty += 1

        if qty >= therapist_qty:
            item[1] = 1

    return create_dict_time(time_list)


def service_json(_id=None, img=None, price=None, minutes=None, percent_for_special_time=None, translations=None):
    return {
        "id": _id,
        "img": img,
        "price": str(price),
        "minutes": minutes,
        "percent_for_special_time":percent_for_special_time,
        "translations": [{
            'lang': translation.lang,
            'title': translation.title,
            'short_desc': translation.short_desc,
            'desc': translation.desc,
        } for translation in translations] if translations else [],
    }


def add_services_json(add_services_to_hotel=None):
    return [{
        "id": add_service.additional_service.id,
        "price": str(add_service.price),
        "translations": [{
            'lang': translation.lang,
            'title': translation.title,
            'desc': translation.desc,
        } for translation in add_service.additional_service.translations.all()] if add_service.additional_service.translations else {}
    } for add_service in add_services_to_hotel] \
        if add_services_to_hotel else [{
        "id": None,
        "price": None,
        "translations": []
    }]


def save_service_json(service: ServiceToHotelModel):
    return service_json(_id=service.service.id,
                        img=service.service.img.url if service.service.img else None,
                        price=service.price,
                        minutes=service.minutes,
                        translations=service.service.translations.all())


def save_add_service_json(add_services_to_hotel):
    return add_services_json(add_services_to_hotel)


def save_price(order):
    price = order.service.price
    for add_service in order.additional_service_json:
        price += Decimal(add_service.get('price')) if add_service.get('price') else 0
    if order.special_time:
        price += price/100*order.hotel.percent_for_special_time
    return price

