from rest_framework import serializers
from user_app.models import UserModel
from .models import OrderModel
from hotel_app.models import *


class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserModel
        fields = ('id', 'first_name', 'last_name', 'email')


class HotelTranslationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = HotelTranslationModel
        fields = "__all__"


class HotelSerializer(serializers.ModelSerializer):
    translations = HotelTranslationsSerializer(many=True)

    class Meta:
        model = HotelModel
        fields = "__all__"


class OrderSerializer(serializers.ModelSerializer):
    therapist = UserModelSerializer(read_only=True)
    therapist_id = serializers.IntegerField(write_only=True, required=False)
    hotel_admin = UserModelSerializer(required=False)
    hotel_admin_id = serializers.IntegerField(write_only=True, required=False)
    book_data_time = serializers.DateTimeField(required=False)
    hotel = HotelSerializer(read_only=True)
    hotel_id = serializers.IntegerField(write_only=True, required=False)
    service_id = serializers.IntegerField(write_only=True, required=False)
    client_cart_number = serializers.CharField(write_only=True, required=False)
    client_validity = serializers.CharField(write_only=True, required=False)
    client_cvv = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = OrderModel
        fields = "__all__"
