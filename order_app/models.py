from django.db import models
from order_app.validators import validate_time
from model_utils.models import TimeStampedModel
from django.utils.translation import gettext as _
from hotel_app.models import HotelAdministratorModel, TherapistModel, HotelModel
from service_app.models import ServiceToHotelModel
from .order_constants import ORDER_STATUSES, AWAITING_CONFIRMATION
from .helper import service_json, add_services_json, save_service_json, save_add_service_json, save_price


class OrderModel(TimeStampedModel):

    class Meta:
        db_table = 'orders'
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')

    hotel_admin = models.ForeignKey(to=HotelAdministratorModel, on_delete=models.SET_NULL, related_name='orders',
                                    verbose_name=_("Hotel administrator"),
                                    null=True, blank=True,
                                    )
    hotel = models.ForeignKey(to=HotelModel, on_delete=models.SET_NULL, related_name='orders',
                                    verbose_name=_("Hotel"),
                                    null=True, blank=True,
                                    )
    therapist = models.ForeignKey(to=TherapistModel, on_delete=models.SET_NULL, related_name='orders',
                                verbose_name=_("Therapist"),
                                null=True, blank=True,
                                )
    service = models.ForeignKey(to=ServiceToHotelModel, on_delete=models.SET_NULL, related_name='orders',
                                verbose_name=_("Service"),
                                null=True, blank=True,
                                )
    service_json = models.JSONField(verbose_name="Service (json)", default=service_json(), null=True, blank=True)
    additional_service_json = models.JSONField(verbose_name="Additional service (json)", default=add_services_json(), null=True, blank=True)
    status = models.CharField(verbose_name=_("Status"), choices=ORDER_STATUSES, default=AWAITING_CONFIRMATION, max_length=255, null=True, blank=True)
    total_price = models.DecimalField(verbose_name=_('Total price'), decimal_places=2, max_digits=10, null=True, blank=True)
    book_data_time = models.DateTimeField(verbose_name=_('Date and time'), validators=[validate_time], null=False, blank=False)
    special_time = models.BooleanField(verbose_name=_("Special time"), default=False)
    client_first_name = models.CharField(verbose_name=_("Client first name"), max_length=255, null=True, blank=True)
    client_last_name = models.CharField(verbose_name=_("Client last name"), max_length=255, null=True, blank=True)
    client_room = models.CharField(verbose_name=_("Client room"), max_length=5, null=True, blank=True)
    client_email = models.EmailField(verbose_name=_("Client email"), max_length=255, null=True, blank=True)
    client_cart_number = models.CharField(verbose_name=_("Client cart number"), max_length=16, null=True, blank=True)
    exp_month = models.CharField(verbose_name=_("Client validity month"), max_length=2, null=True, blank=True)
    exp_year = models.CharField(verbose_name=_("Client validity year"), max_length=2, null=True, blank=True)
    client_cvv = models.CharField(verbose_name=_("Client cvv"), max_length=3, null=True, blank=True)
    pay_by_cart = models.BooleanField(verbose_name=_('Pay by cart'), default=False, null=False, blank=False)
    canceled = models.BooleanField(verbose_name=_('Canceled'), default=False, null=False, blank=False)
    comment = models.TextField(verbose_name=_("Comment"), null=True, blank=True)

    def __str__(self):
        return f"{self.hotel} - {self.hotel_admin} - {self.book_data_time} - {self.total_price}"


    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.service:
            self.service_json = save_service_json(self.service)
        super().save()


class ProfilePay(TimeStampedModel):
    class Meta:
        db_table = 'profile_pay'
        verbose_name = _('profile_pay')
        verbose_name_plural = _('profile_pay')

    date = models.DateTimeField(verbose_name=_('Date and time'), auto_now_add=True)
    pay_id = models.TextField(verbose_name=_("Pay id"), null=True, blank=True)
    data_pay_json = models.JSONField(verbose_name="Data pay (json)", null=True, blank=True)
    status_pay_success = models.BooleanField(verbose_name=_("Status pay success"), default=False)
    order = models.ForeignKey(to=OrderModel, verbose_name='Order', null=True, blank=True, default=None,
                              related_name="pay_order", on_delete=models.SET_NULL)
    payment_method_id = models.CharField(verbose_name=_("Payment method id"), max_length=300, null=True, blank=True)

    def __str__(self):
        return f"{self.date} - {self.status_pay_success} - {self.order}"

