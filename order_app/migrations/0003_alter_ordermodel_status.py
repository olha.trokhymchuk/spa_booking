# Generated by Django 3.2.6 on 2021-08-18 11:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order_app', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ordermodel',
            name='status',
            field=models.CharField(blank=True, choices=[('all', 'all'), ('active', 'active'), ('past', 'past')], max_length=255, null=True, verbose_name='Status'),
        ),
    ]
