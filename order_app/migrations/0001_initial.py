# Generated by Django 3.2.6 on 2021-08-16 15:04

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields
import order_app.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OrderModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('service_json', models.JSONField(blank=True, null=True, verbose_name='Service (json)')),
                ('additional_service_json', models.JSONField(blank=True, null=True, verbose_name='Additional service (json)')),
                ('status', models.CharField(blank=True, max_length=255, null=True, verbose_name='Status')),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Total price')),
                ('book_data_time', models.DateTimeField(validators=[order_app.validators.validate_time], verbose_name='Date and time')),
                ('special_time', models.BooleanField(default=False, verbose_name='Special time')),
                ('client_first_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Client first name')),
                ('client_last_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Client last name')),
                ('client_room', models.CharField(blank=True, max_length=5, null=True, verbose_name='Client room')),
                ('client_email', models.EmailField(blank=True, max_length=255, null=True, verbose_name='Client email')),
                ('client_cart_number', models.CharField(blank=True, max_length=16, null=True, verbose_name='Client cart number')),
                ('client_validity', models.CharField(blank=True, max_length=5, null=True, verbose_name='Client validity')),
                ('client_cvv', models.CharField(blank=True, max_length=3, null=True, verbose_name='Client cvv')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='Comment')),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
                'db_table': 'orders',
            },
        ),
    ]
