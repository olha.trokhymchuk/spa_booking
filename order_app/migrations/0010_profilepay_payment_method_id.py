# Generated by Django 3.2.6 on 2021-09-10 11:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order_app', '0009_alter_ordermodel_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilepay',
            name='payment_method_id',
            field=models.CharField(blank=True, max_length=300, null=True, verbose_name='Payment method id'),
        ),
    ]
