# Generated by Django 3.2.6 on 2021-09-01 11:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order_app', '0005_profilepay'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilepay',
            name='order',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='pay_order', to='order_app.ordermodel', verbose_name='Order'),
        ),
    ]
