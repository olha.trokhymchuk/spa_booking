from django.utils.translation import gettext as _

# ALL = 'all'
ACTIVE = 'active'
PAST = 'past'
AWAITING_CONFIRMATION = 'awaiting_confirmation'
# ON_CONTRACT = 'on_contract'
# CANCELLED = 'cancelled'

ORDER_STATUSES = [
    (ACTIVE, _('Active')),
    (PAST, _('Past')),
    (AWAITING_CONFIRMATION, _('Awaiting confirmation')),
]
