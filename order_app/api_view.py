import datetime
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from spa_booking.custom_api_exeptions import Http400
from .models import *
from .helper import get_list_for_time_piker
from .serializers import OrderSerializer, UserModelSerializer
from service_app.models import AdditionalServiceToServiceInHotelModel


class TimeListApiListView(generics.ListAPIView):
    queryset = OrderModel.objects.all()
    def list(self, request, *args, **kwargs):
        if "id" not in self.request.query_params:
            raise Http400(detail="id - required field.")
        if "date" not in self.request.query_params:
            raise Http400(detail="date - required field.")
        _id = self.request.query_params.get('id')
        hotel = get_object_or_404(HotelModel.objects.all(), id=_id)
        date = self.request.query_params.get('date')
        return Response(
            get_list_for_time_piker(
                hotel=hotel,
                order_list=OrderModel.objects.filter(hotel=hotel,
                                                     book_data_time__date=date,
                                                     status='active'),
                therapist_qty=TherapistModel.objects.filter(hotel=hotel).count(),
                date_day=date)
        )


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'page_num': self.page.paginator.num_pages,
            'results': data
        })


class OrdersListApiView(generics.ListAPIView, generics.UpdateAPIView, generics.CreateAPIView):
    serializer_class = OrderSerializer
    pagination_class = StandardResultsSetPagination
    queryset = OrderModel.objects.all()
    model = OrderModel
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['hotel', 'status', 'hotel_admin', 'therapist', 'pay_by_cart', 'canceled']

    def get_queryset(self):
        queryset = super().get_queryset()
        print(queryset)
        if "book_data_time" in self.request.query_params and self.request.query_params.get('book_data_time')!="":
            queryset = queryset.filter(book_data_time__date=self.request.query_params.get('book_data_time'))
        if "default" in self.request.query_params:
            queryset = queryset.filter(book_data_time__date=self.request.query_params.get('book_data_time')).order_by("-id")
        return queryset

    def perform_update(self, serializer):
        if 'add_service_list' in self.request.data:
            serializer.validated_data['additional_service_json'] = save_add_service_json(
                AdditionalServiceToServiceInHotelModel.objects.filter(id__in=self.request.data.get('add_service_list')))
        serializer.save()
        instance = OrderModel.objects.get(id=serializer.data.get('id'))
        instance.total_price = save_price(instance)
        instance.save()

    def perform_create(self, serializer):
        if 'add_service_list' in self.request.data:
            serializer.validated_data['additional_service_json'] = save_add_service_json(
                    AdditionalServiceToServiceInHotelModel.objects.filter(id__in=self.request.data.get('add_service_list')))
        serializer.save()
        instance = OrderModel.objects.get(id=serializer.data.get('id'))
        instance.total_price = save_price(instance)
        instance.save()


class TherapistsListView(generics.ListAPIView):
    serializer_class = UserModelSerializer
    queryset = TherapistModel.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        if "order_id" not in self.request.query_params:
            raise Http400(detail="order_id - required field.")
        order_id = self.request.query_params.get('order_id')
        order = get_object_or_404(OrderModel.objects.all(), id=order_id)
        hotel = get_object_or_404(HotelModel.objects.all(), orders=order)
        queryset.filter(hotel=hotel)
        return queryset
