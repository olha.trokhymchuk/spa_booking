import json
from django.utils.translation import gettext as _
from django.contrib import admin
from django_google_maps import widgets as map_widgets
from .forms import *
from .models import *
from django.contrib.auth.admin import UserAdmin


class HotelTranslationInLineaAdminModel(admin.StackedInline):
    model = HotelTranslationModel
    extra = len(LANG)
    max_num = len(LANG)


class HotelAdminModel(admin.ModelAdmin):
    search_fields = ('translations__title', 'address')
    list_display = ('id', 'title_en', 'title_de', 'address', 'therapist_qty', 'services_qty')

    def title_en(self, obj):
        return obj.translations.get(lang=EN).title if obj.translations.get(lang=EN) else '-'

    title_en.short_description = _('TITLE (EN)')

    def title_de(self, obj):
        return obj.translations.get(lang=DE).title if obj.translations.get(lang=DE) else '-'

    title_de.short_description = _('TITLE (DE)')

    def therapist_qty(self, obj):
        return obj.therapists.count()

    therapist_qty.short_description = _('COUNT OF THERAPISTS')

    def services_qty(self, obj):
        return obj.services.count()

    services_qty.short_description = _('COUNT OF SERVICES')

    inlines = (
        HotelTranslationInLineaAdminModel,
    )

    formfield_overrides = {
        map_fields.AddressField:
            {'widget':
                map_widgets.GoogleMapsAddressWidget(attrs={
                    'data-autocomplete-options': json.dumps({'types': ('geocode', 'establishment'),
                                                             # 'componentRestrictions': {
                                                             #     'country': '',
                                                             #    },
                                                             })
                })
            },
    }

    fieldsets = (
        (_('Image'), {
            'fields': ('img',)
        }),
        (_('SPA Menu'), {
            'fields': ('pdf_file', 'percent_for_special_time')
        }),
        (_('Spa reception'), {
            'fields': ('spa_reception_work_time_from', 'spa_reception_work_time_to',)
        }),
        (_('Spa services'), {
            'fields': ('spa_service_work_time_from', 'spa_service_work_time_to')
        }),
        (_('Spa facilities'), {
            'fields': ('spa_facilities_work_time_from', 'spa_facilities_work_time_to')
        }),
        (_('Rhythm & Hair'), {
            'fields': ('rhythm_hair_work_time_from', 'rhythm_hair_work_time_to')
        }),
        (_('Address'), {
            'fields': ('address', 'geolocation')
        }),
    )


class TherapistAdminModel(UserAdmin):
    add_form = TherapistCreationForm
    form = TherapistChangeForm
    model = TherapistModel
    ordering = ('email',)
    list_display_links = ('first_name', 'last_name', 'email')
    list_display = ('first_name', 'last_name', 'email', 'is_active')
    exclude = ('last_login', 'is_superuser', 'is_staff', 'date_joined', 'groups', 'permissions', 'username')
    list_filter = ('hotel',)
    search_fields = ('first_name', 'last_name', 'email')
    fieldsets = (
        (_('Log in'), {
            'fields': ('email', 'password')
        }),
        (_('Therapist info'), {
            'fields': ('first_name', 'last_name', 'img', 'hotel')
        }),
        (_('For delete therapist'), {
            'classes': ('collapse',),
            'fields': ('is_active', )
        }),
    )
    add_fieldsets = (
        ('Credentials', {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )


class HotelAdministratorAdminModel(UserAdmin):
    add_form = HotelAdministratorCreationForm
    form = HotelAdministratorChangeForm
    model = HotelAdministratorModel
    ordering = ('email',)
    list_display_links = ('first_name', 'last_name', 'email')
    list_display = ('first_name', 'last_name', 'email', 'hotel', 'is_active')
    exclude = ('last_login', 'is_superuser', 'is_staff', 'date_joined', 'groups', 'permissions', 'username')
    list_filter = ('hotel',)
    search_fields = ('first_name', 'last_name', 'email')
    fieldsets = (
        (_('Log in'), {
            'fields': ('email', 'password')
        }),
        (_('Therapist info'), {
            'fields': ('first_name', 'last_name', 'img', 'hotel')
        }),
        (_('For delete hotel admin'), {
            'classes': ('collapse',),
            'fields': ('is_active',)
        }),
    )
    add_fieldsets = (
        (_('Credentials'), {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )


admin.site.register(HotelModel, HotelAdminModel)
admin.site.register(TherapistModel, TherapistAdminModel)
admin.site.register(HotelAdministratorModel, HotelAdministratorAdminModel)
