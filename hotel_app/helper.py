

def list_of_hotels_with_translation(hotels, lang):
    hotels_resp = list()
    for hotel in hotels:
        translation = hotel.translations.filter(lang__iregex=lang).first()
        hotels_resp += [
            {
                'id': hotel.id,
                'img': hotel.img.url if hotel.img else "",
                "address": hotel.address,
                "geolocation": str(hotel.geolocation),
                'translations': {
                    'lang': translation.lang,
                    'title': translation.title,
                    'short_desc': translation.short_desc,
                    'desc': translation.desc,

                } if translation else {},
            }
        ]
    return hotels_resp
