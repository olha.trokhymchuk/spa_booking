from django.urls import path
from .api_views import *

urlpatterns = [
    path('hotels/', HotelsApiListView.as_view(), name="hotel_api_list"),
]
