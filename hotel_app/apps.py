from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HotelAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hotel_app'
    verbose_name = _('Hotel')
