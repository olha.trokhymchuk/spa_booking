from django.db import models
from hotel_app.validators import validate_time
from spa_booking.validators import ImageValidator
from user_app.models import UserModel
from spa_booking.constants import *
from model_utils.models import TimeStampedModel
from django.utils.translation import gettext as _
from django_google_maps import fields as map_fields


class HotelModel(TimeStampedModel):

    class Meta:
        db_table = 'hotels'
        verbose_name = _('Hotel')
        verbose_name_plural = _('Hotels')

    spa_reception_work_time_from = models.TimeField(verbose_name=_('Work from'), validators=[validate_time], null=True, blank=True)
    spa_reception_work_time_to = models.TimeField(verbose_name=_('Work to'), validators=[validate_time], null=True, blank=True)
    spa_service_work_time_from = models.TimeField(verbose_name=_('Work from'), validators=[validate_time], null=True, blank=True)
    spa_service_work_time_to = models.TimeField(verbose_name=_('Work to'), validators=[validate_time], null=True, blank=True)
    spa_facilities_work_time_from = models.TimeField(verbose_name=_('Work from'), validators=[validate_time], null=True, blank=True)
    spa_facilities_work_time_to = models.TimeField(verbose_name=_('Work to'), validators=[validate_time], null=True, blank=True)
    rhythm_hair_work_time_from = models.TimeField(verbose_name=_('Work from'), null=True, blank=True)
    rhythm_hair_work_time_to = models.TimeField(verbose_name=_('Work to'), null=True, blank=True)
    percent_for_special_time = models.PositiveIntegerField(verbose_name=_("Percent for special time"), default=10)
    img = models.ImageField(verbose_name=_('Image'), upload_to="hotel/img", validators=[ImageValidator(size=256000, width=1000, height=1000)], null=True, blank=True)
    address = map_fields.AddressField(verbose_name=_('Address'), max_length=255, null=False, blank=False, default='')
    geolocation = map_fields.GeoLocationField(verbose_name=_('Geolocation'), max_length=255, null=False, blank=False, default='')
    pdf_file = models.FileField(verbose_name=_('File PDF'), upload_to='hotel/file_pdf', null=True, blank=True)

    def __str__(self):
        return f'{self.translations.get(lang=EN).title if self.translations.filter(lang=EN) else "-"} / ' \
               f'{self.translations.get(lang=DE).title if self.translations.filter(lang=DE) else "-"}'


class HotelTranslationModel(TimeStampedModel):

    class Meta:
        db_table = 'hotel_translation'
        verbose_name = _('Hotel translation')
        verbose_name_plural = _('Hotels translation')
        constraints = [
            models.UniqueConstraint(fields=['hotel', 'lang'], name='unique_hotel_lang_translation')
        ]

    hotel = models.ForeignKey(to="HotelModel", on_delete=models.CASCADE, related_name='translations',
                              verbose_name=_("Hotel"),
                              null=False, blank=False,
                              )
    lang = models.CharField(verbose_name=_("Language"), choices=LANG, max_length=2, null=False, blank=False)
    title = models.CharField(verbose_name=_("Title"), max_length=255, null=False, blank=False)
    short_desc = models.TextField(verbose_name=_("Short description"), null=False, blank=False)
    desc = models.TextField(verbose_name=_('Description'), null=False, blank=False)


class TherapistModel(UserModel):

    class Meta:
        db_table = 'therapist'
        verbose_name = _('Therapist')
        verbose_name_plural = _('Therapists')

    hotel = models.ManyToManyField(to="HotelModel", related_name='therapists', verbose_name=_("Hotel"))
    img = models.ImageField(verbose_name=_('Photo'), upload_to="therapist/img", validators=[ImageValidator(size=256000, width=1000, height=1000)], null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class HotelAdministratorModel(UserModel):

    class Meta:
        db_table = 'hotel_administrator'
        verbose_name = _('Hotel administrator')
        verbose_name_plural = _('Hotel administrators')

    hotel = models.OneToOneField(to="HotelModel", on_delete=models.SET_NULL, related_name='administrators',
                                verbose_name=_("Hotel"),
                                null=True, blank=True,
                            )

    img = models.ImageField(verbose_name=_('Photo'), upload_to="hotel_admin/img", validators=[ImageValidator(size=256000, width=1000, height=1000)], null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'




