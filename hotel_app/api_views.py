from rest_framework import generics
from rest_framework.response import Response
from spa_booking.custom_api_exeptions import Http400
from .models import *
from .helper import list_of_hotels_with_translation


class HotelsApiListView(generics.ListAPIView):
    queryset = HotelModel.objects.all()

    def list(self, request, *args, **kwargs):
        if "lang" not in self.request.query_params:
            raise Http400(detail="lang - required field.")
        lang = self.request.query_params.get('lang')
        return Response(list_of_hotels_with_translation(HotelModel.objects.all(), lang))
